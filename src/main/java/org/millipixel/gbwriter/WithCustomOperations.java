package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.custom.CartridgeHeader;
import org.millipixel.gbwriter.operation.custom.Db;
import org.millipixel.gbwriter.operation.custom.Dw;
import org.millipixel.gbwriter.operation.custom.Reserve;

public interface WithCustomOperations {

    Section getCurrent();

    default void reserve(int size) {
        getCurrent().addOperation(new Reserve(0x00, size));
    }

    default void reserve(int size, int value) {
        getCurrent().addOperation(new Reserve(value, size));
    }

    default void cartridgeHeader() {
        getCurrent().addOperation(new CartridgeHeader());
    }

    default void db(int ... values) {
        getCurrent().addOperation(new Db(values));
    }

    default void dw(String str) {
        getCurrent().addOperation(new Dw(str));
    }
}
