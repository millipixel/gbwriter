package org.millipixel.gbwriter;

public interface Id {
    interface Identifier {
        String name();
    }
    interface R8 extends Identifier {}
    interface Flag extends Identifier {}
    interface R16 extends Identifier {}

    //////////////////////
    // 8 bytes registers
    //////////////////////
    interface A_B_C_D_E_H_L_HLa extends Identifier {}
    interface A_B_C_D_E_H_L extends Identifier {}
    class A implements R8, A_B_C_D_E_H_L_HLa, A_B_C_D_E_H_L {
        @Override public String name() {
            return "A";
        }
    }
    class B implements R8, A_B_C_D_E_H_L_HLa, A_B_C_D_E_H_L {
        @Override public String name() {
            return "B";
        }
    }
    class C implements R8, A_B_C_D_E_H_L_HLa, A_B_C_D_E_H_L, Flag {
        @Override public String name() {
            return "C";
        }
    }
    class D implements R8, A_B_C_D_E_H_L_HLa, A_B_C_D_E_H_L {
        @Override public String name() {
            return "D";
        }
    }
    class E implements R8, A_B_C_D_E_H_L_HLa, A_B_C_D_E_H_L {
        @Override public String name() {
            return "E";
        }
    }
    class H implements R8, A_B_C_D_E_H_L_HLa, A_B_C_D_E_H_L {
        @Override public String name() {
            return "H";
        }
    }
    class L implements R8, A_B_C_D_E_H_L_HLa, A_B_C_D_E_H_L {
        @Override public String name() {
            return "L";
        }
    }

    //////////////////////
    // 16 bytes registers
    //////////////////////
    interface BC_DE_HLI_HLD extends Identifier {}
    interface BC_DE_HL_SP extends Identifier {}
    interface BC_DE_HL_AF extends Identifier {}
    class BC implements R16, BC_DE_HLI_HLD, BC_DE_HL_SP, BC_DE_HL_AF {
        @Override public String name() {
            return "BC";
        }
    }
    class DE implements R16, BC_DE_HLI_HLD, BC_DE_HL_SP, BC_DE_HL_AF {
        @Override public String name() {
            return "DE";
        }
    }
    class HL implements R16, BC_DE_HL_SP, BC_DE_HL_AF {
        @Override public String name() {
            return "HL";
        }
    }
    class HLa implements R16, A_B_C_D_E_H_L_HLa {
        @Override public String name() {
            return "HLa";
        }
    }
    class HLD implements R16, BC_DE_HLI_HLD {
        @Override public String name() {
            return "HLD";
        }
    }
    class HLI implements R16, BC_DE_HLI_HLD {
        @Override public String name() {
            return "HLI";
        }
    }
    class SP implements R16, BC_DE_HL_SP {
        @Override public String name() {
            return "SP";
        }
    }
    class AF implements R16, BC_DE_HL_AF {
        @Override public String name() {
            return "AF";
        }
    }

    //////////////////////
    // Flags
    //////////////////////

    class NC implements Flag {
        @Override public String name() {
            return "NC";
        }
    }
    class Z implements Flag {
        @Override public String name() {
            return "Z";
        }
    }
    class NZ implements Flag {
        @Override public String name() {
            return "NZ";
        }
    }
}
