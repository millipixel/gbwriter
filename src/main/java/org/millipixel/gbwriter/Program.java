package org.millipixel.gbwriter;

import groovy.lang.Closure;
//import org.millipixel.gbwriter.identifier.*;
import org.millipixel.gbwriter.keyword.*;
import org.millipixel.gbwriter.operation.custom.CartridgeHeader;
import org.millipixel.gbwriter.operation.custom.Db;
import org.millipixel.gbwriter.operation.custom.Dw;
import org.millipixel.gbwriter.operation.custom.Reserve;
import org.millipixel.gbwriter.operation.legacy.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/*
public class Program implements WithFunctions {
    public final Rom0 ROM0 = new Rom0();
    public final WRam0 WRAM0 = new WRam0();

    private List<Section> sections = new ArrayList<>();
    private Section current;

    public Section SECTION(String name, SectionType type, int off) {
        Section section = new Section(name, type, off);
        sections.add(section);
        current = section;
        return section;
    }

    public Section SECTION(String name, SectionType type) {
        Section section = new Section(name, type);
        sections.add(section);
        current = section;
        return section;
    }

    @Override
    public String toString(){
        StringBuilder ret = new StringBuilder();
        for(Section s : sections) {
            ret.append(s).append("\n");
        }
        return ret.toString();
    }

    public List<Byte> toBytes() {
        int off = 0;
        for(Section s : sections) {
            off = s.calcOff(off);
        }

        List<Byte> ret = new ArrayList<>();
        for(Section s : sections) {
            ret.addAll(s.toBytes(sections));
        }
        return ret;
    }

    public void writeIn(String filePath) {
        byte[] bytes = new byte[0x4000];
        Arrays.fill(bytes, (byte)0x00);
        int start = sections.get(0).getStart();
        List<Byte> byteList = toBytes();
        for (int i = 0; i < byteList.size(); i++) {
            bytes[i+start] = byteList.get(i);
        }
        try(FileOutputStream fos = new FileOutputStream(new File(filePath))) {
            fos.write(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Section getCurrent() {
        return current;
    }
}
*/