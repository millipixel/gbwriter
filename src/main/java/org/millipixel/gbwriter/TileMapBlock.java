package org.millipixel.gbwriter;

public enum TileMapBlock {
    TILEMAP_BLOCK_1(0x9800),
    TILEMAP_BLOCK_2(0x9C00);

    TileMapBlock(int offset) {
        this.offset = offset;
    }

    private final int offset;

    public int getOffset() {
        return offset;
    }
}
