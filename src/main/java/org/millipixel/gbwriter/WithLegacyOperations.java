package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.*;

public interface WithLegacyOperations {

    Section getCurrent();

    default void adc(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Adc(r));
    }

    default void adc(byte b) {
        getCurrent().addOperation(new Adc(b));
    }

    default void add(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Add(r));
    }

    default void add(byte b) {
        getCurrent().addOperation(new Add(b));
    }

    default void add(Id.SP sp, byte b) {
        getCurrent().addOperation(new Add(sp, b));
    }

    default void add(Id.HL ignored, Id.BC_DE_HL_SP r) {
        getCurrent().addOperation(new Add(ignored, r));
    }

    default void and(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Add(r));
    }

    default void and(byte b) {
        getCurrent().addOperation(new Add(b));
    }

    default void bit(Id.A_B_C_D_E_H_L_HLa r, int i) {
        getCurrent().addOperation(new Bit(r, i));
    }

    default void calll(Id.Flag f, int v) {
        getCurrent().addOperation(new Call(f, v));
    }

    default void calll(int v) {
        getCurrent().addOperation(new Call(v));
    }

    default void calll(Id.Flag f, String l) {
        getCurrent().addOperation(new Call(f, l));
    }

    default void calll(String l) {
        getCurrent().addOperation(new Call(l));
    }

    default void ccf() {
        getCurrent().addOperation(new CCF());
    }

    default void cp(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Cp(r));
    }

    default void cp(int b) {
        getCurrent().addOperation(new Cp((byte)b));
    }

    default void cpl() {
        getCurrent().addOperation(new CPL());
    }

    default void daa() {
        getCurrent().addOperation(new DAA());
    }

    default void dec(Id.Identifier r) {
        getCurrent().addOperation(new Dec(r));
    }

    default void di() {
        getCurrent().addOperation(new DI());
    }

    default void ei() {
        getCurrent().addOperation(new EI());
    }

    default void halt() {
        getCurrent().addOperation(new Halt());
    }

    default void inc(Id.Identifier r) {
        getCurrent().addOperation(new Inc(r));
    }

    default void jp(String l) {
        getCurrent().addOperation(new Jp(l));
    }

    default void jp(Id.Flag f, String l) {
        getCurrent().addOperation(new Jp(f, l));
    }

    default void jr(Byte b) {
        getCurrent().addOperation(new Jr(b));
    }

    default void jr(Id.Flag f, Byte b) {
        getCurrent().addOperation(new Jr(f, b));
    }

    default void ld(Id.BC_DE_HL_SP left, Integer w) {
        getCurrent().addOperation(new Ld(left, w));
    }
    default void ld(Id.BC_DE_HL_SP left, String label) {
        getCurrent().addOperation(new Ld(left, label));
    }
    default void ld(Id.BC_DE_HL_SP left, String start, String end) {
        getCurrent().addOperation(new Ld(left, start, end));
    }
    default void ld(Id.BC_DE_HLI_HLD left, Id.A right) {
        getCurrent().addOperation(new Ld(left, right));
    }
    default void ld(Id.A_B_C_D_E_H_L_HLa left, int b) {
        getCurrent().addOperation(new Ld(left, (byte)b));
    }
    default void ld(int w, Id.SP right) {
        getCurrent().addOperation(new Ld(w, right));
    }
    default void ld(Id.A left, Id.BC_DE_HLI_HLD right) {
        getCurrent().addOperation(new Ld(left, right));
    }
    default void ld(Id.A_B_C_D_E_H_L left, Id.A_B_C_D_E_H_L_HLa right) {
        getCurrent().addOperation(new Ld(left, right));
    }
    default void ld(Id.HLa left, Id.A_B_C_D_E_H_L right) {
        getCurrent().addOperation(new Ld(left, right));
    }
    default void ld(Id.HL left, Id.SP right, byte b) {
        getCurrent().addOperation(new Ld(left, right, b));
    }
    default void ld(Id.SP left, Id.HL right) {
        getCurrent().addOperation(new Ld(left, right));
    }
    default void ld(int w, Id.A right) {
        getCurrent().addOperation(new Ld(w, right));
    }
    default void ld(Id.A left, Integer w) {
        getCurrent().addOperation(new Ld(left, w));
    }
    default void ld(Id.A left, String label) {
        getCurrent().addOperation(new Ld(left, label));
    }
    default void ld(Id.A left, String start, String end) {
        getCurrent().addOperation(new Ld(left, start, end));
    }

    default void ldh(int b, boolean left) {
        getCurrent().addOperation(new Ldh(b, left));
    }

    default void nop() {
        getCurrent().addOperation(new Nop());
    }

    default void or(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Or(r));
    }

    default void or(Byte b) {
        getCurrent().addOperation(new Or(b));
    }

    default void pop(Id.BC_DE_HL_AF r) {
        getCurrent().addOperation(new Pop(r));
    }

    default void push(Id.BC_DE_HL_AF r){
        getCurrent().addOperation(new Push(r));
    }

    default void res(Id.A_B_C_D_E_H_L_HLa r, int i){
        getCurrent().addOperation(new Res(r, i));
    }
    default void ret(Id.Flag flag){
        getCurrent().addOperation(new Ret(flag));
    }

    default void ret(){
        getCurrent().addOperation(new Ret());
    }

    default void reti(){
        getCurrent().addOperation(new Reti());
    }

    default void rl(Id.A_B_C_D_E_H_L_HLa r){
        getCurrent().addOperation(new Rl(r));
    }

    default void rla(){
        getCurrent().addOperation(new RLA());
    }

    default void rlc(Id.A_B_C_D_E_H_L_HLa r){
        getCurrent().addOperation(new Rlc(r));
    }

    default void rlca(){
        getCurrent().addOperation(new RLCA());
    }
    default void rr(Id.A_B_C_D_E_H_L_HLa r){
        getCurrent().addOperation(new Rr(r));
    }

    default void rra(){
        getCurrent().addOperation(new RRA());
    }

    default void rrc(Id.A_B_C_D_E_H_L_HLa r){
        getCurrent().addOperation(new Rrc(r));
    }

    default void rrca() {
        getCurrent().addOperation(new RRCA());
    }

    default void rst(int i) {
        getCurrent().addOperation(new RST(i));
    }

    default void sbc(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Sbc(r));
    }

    default void sbc(Byte b) {
        getCurrent().addOperation(new Sbc(b));
    }

    default void scf() {
        getCurrent().addOperation(new SCF());
    }

    default void set(Id.A_B_C_D_E_H_L_HLa r, int i) {
        getCurrent().addOperation(new Set(r, i));
    }

    default void sla(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Sla(r));
    }

    default void sra(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Sra(r));
    }

    default void srl(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Srl(r));
    }

    default void stop() {
        getCurrent().addOperation(new Stop());
    }

    default void sub(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Sub(r));
    }

    default void sub(Byte b) {
        getCurrent().addOperation(new Sub(b));
    }

    default void swap(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Swap(r));
    }

    default void xor(Id.A_B_C_D_E_H_L_HLa r) {
        getCurrent().addOperation(new Xor(r));
    }
}
