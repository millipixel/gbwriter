package org.millipixel.gbwriter;


public enum TileBlock {
    TILE_BLOCK_1(0x8000),
    TILE_BLOCK_2(0x8800),
    TILE_BLOCK_3(0x9000);

    TileBlock(int offset) {
        this.offset = offset;
    }

    private final int offset;

    public int getOffset() {
        return offset;
    }
}
