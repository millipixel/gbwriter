package org.millipixel.gbwriter;

public interface WithConstants {
    /**
     * AUDENA/NR52 (0xFF26)
     * Sound on/off (R/W)
     * Bit 7   - All sound on/off (sets all audio regs to 0!)
     * Bit 3   - Sound 4 ON flag (read only)
     * Bit 2   - Sound 3 ON flag (read only)
     * Bit 1   - Sound 2 ON flag (read only)
     * Bit 0   - Sound 1 ON flag (read only)
     */
    int NR52 = 0xFF26;//ARTHUR P
    int AUDENA = NR52;
    byte AUDENA_ON = (byte)0b10000000;
    byte AUDENA_OFF = 0b00000000;

    /**
     * LY (0xFF44)
     * LCDC Y-Coordinate (R)
     * Values range from 0->153. 144->153 is the VBlank period.
     */
    int LY = 0xFF44;//SOPHIE P

    /**
     * LCDC (0xFF40)
     * LCD Control (R/W)
     */
    int LCDC = 0xFF40;

    int LCDCF_OFF     = 0b00000000; // LCD Control Operation
    int LCDCF_ON      = 0b10000000; // LCD Control Operation
    int LCDCF_WIN9800 = 0b00000000; // Window Tile Map Display Select
    int LCDCF_WIN9C00 = 0b01000000; // Window Tile Map Display Select
    int LCDCF_WINOFF  = 0b00000000; // Window Display
    int LCDCF_WINON   = 0b00100000; // Window Display
    int LCDCF_BG8800  = 0b00000000; // BG & Window Tile Data Select
    int LCDCF_BG8000  = 0b00010000; // BG & Window Tile Data Select
    int LCDCF_BG9800  = 0b00000000; // BG Tile Map Display Select
    int LCDCF_BG9C00  = 0b00001000; // BG Tile Map Display Select
    int LCDCF_OBJ8    = 0b00000000; // OBJ Construction
    int LCDCF_OBJ16   = 0b00000100; // OBJ Construction
    int LCDCF_OBJOFF  = 0b00000000; // OBJ Display
    int LCDCF_OBJON   = 0b00000010; // OBJ Display
    int LCDCF_BGOFF   = 0b00000000; // BG Display
    int LCDCF_BGON    = 0b00000001; // BG Display

    /**
     * BGP (0xFF47)
     * BG Palette Data (W)
     * Bit 7-6 - Intensity for %11
     * Bit 5-4 - Intensity for %10
     * Bit 3-2 - Intensity for %01
     * Bit 1-0 - Intensity for %00
     */
    int BGP = 0xFF47;
    int OBP0 = 0xFF48;
    int OBP1 = 0xFF49;

    int OAMRAM = 0xFE00;

    /**
     * P1 (0xFF00)
     * Register8 for reading joy pad info. (R/W)
     */
    int P1 = 0xFF00;

    int P1F_5 = 0b00100000; // P15 out port, set to 0 to get buttons
    int P1F_4 = 0b00010000; // P14 out port, set to 0 to get dpad
    int P1F_3 = 0b00001000; // P13 in port
    int P1F_2 = 0b00000100; // P12 in port
    int P1F_1 = 0b00000010; // P11 in port
    int P1F_0 = 0b00000001; // P10 in port

    int P1F_GET_DPAD = P1F_5;
    int P1F_GET_BTN  = P1F_4;
    int P1F_GET_NONE = P1F_4 | P1F_5;


    /**
     * Keypad related
     */

    int PADF_DOWN   = 0x80;
    int PADF_UP     = 0x40;
    int PADF_LEFT   = 0x20;
    int PADF_RIGHT  = 0x10;
    int PADF_START  = 0x08;
    int PADF_SELECT = 0x04;
    int PADF_B      = 0x02;
    int PADF_A      = 0x01;

    int PADB_DOWN   = 0x7;
    int PADB_UP     = 0x6;
    int PADB_LEFT   = 0x5;
    int PADB_RIGHT  = 0x4;
    int PADB_START  = 0x3;
    int PADB_SELECT = 0x2;
    int PADB_B      = 0x1;
    int PADB_A      = 0x0;
}
