package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Rom0;

public interface WithIdentifiers {

    Id.A A = new Id.A();
    Id.B B = new Id.B();
    Id.C C = new Id.C();
    Id.D D = new Id.D();
    Id.E E = new Id.E();
    Id.H H = new Id.H();
    Id.L L = new Id.L();

    Id.NC NC = new Id.NC();
    Id.Z Z = new Id.Z();
    Id.NZ NZ = new Id.NZ();

    Id.BC BC = new Id.BC();
    Id.DE DE = new Id.DE();
    Id.HL HL = new Id.HL();
    Id.HLa HLa = new Id.HLa();
    Id.HLI HLI = new Id.HLI();
    Id.HLD HLD = new Id.HLD();
}
