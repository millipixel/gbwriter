package org.millipixel.gbwriter.keyword;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import groovy.transform.stc.ClosureParams;
import groovy.transform.stc.FirstParam;
//import org.millipixel.gbwriter.WithFunctions;
import org.millipixel.gbwriter.WithLegacyOperations;
import org.millipixel.gbwriter.operation.Operation;

import java.util.ArrayList;
import java.util.List;

public class Section /*implements WithFunctions*/ {
    private final String name;
    private final SectionType sectionType;
    private final List<Label> labels;
    private final List<Variable> variables;
    private int start;
    private int end;

    public Section(String name, SectionType sectionType, int off) {
        this.name = name;
        this.sectionType = sectionType;
        this.labels = new ArrayList<>();
        this.variables = new ArrayList<>();
        this.start = off;
        addLabel(new Label(""));
    }

    public Section(String name, SectionType sectionType) {
        this.name = name;
        this.sectionType = sectionType;
        this.labels = new ArrayList<>();
        this.variables = new ArrayList<>();
        addLabel(new Label(""));
    }

    public void addVariable(Variable variable) {
        this.variables.add(variable);
    }

    public Variable getVariable(String name) {
        for(Variable v : variables) {
            if(v.getName().equals(name)) return v;
        }
        return null;
    }

    public List<Variable> getVariables() {
        return variables;
    }

    public void addLabel(Label label) {
        this.labels.add(label);
    }

    public Label getLabel(String name) {
        for(Label l : labels) {
            if(l.getName().equals(name)) return l;
        }
        return null;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public int getStart() {
        return start;
    }

    public SectionType getSectionType() {
        return sectionType;
    }

    public void addOperation(Operation op) {
        labels.getLast().addOperation(op);
    }

    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append("SECTION \"").append(name).append("\", $sectionType.name[$").append(sectionType.getOffset()).append("]\n");
        for(Label l : labels) {
            ret.append(l.toString());
        }
        return ret.toString();
    }

    public List<Byte> toBytes(List<Section> sections) {
        List<Byte> ret = new ArrayList<>();
        for(int i=0; i<sectionType.getOffset(); i++) {
            ret.add((byte) 0x00);
        }
        for(Label l : labels) {
            ret.addAll(l.toBytes(sections));
        }
        return ret;
    }

    public int calcOff(int offset) {
        start += offset;
        int off = start;
        for(Label l : labels) {
            l.setOffset(off);
            off += l.length();
        }
        end = off;
        return off;
    }

    /*@Override
    public Section getCurrent() {
        return this;
    }*/
}