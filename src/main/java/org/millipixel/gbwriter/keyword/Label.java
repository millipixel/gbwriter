package org.millipixel.gbwriter.keyword;

import org.millipixel.gbwriter.operation.Operation;

import java.util.ArrayList;
import java.util.List;

public class Label {
    private final String name;
    private final List<Operation> operations = new ArrayList<>();
    private int offset = 0;

    public Label(String name) {
        this.name = name;
    }

    public void addOperation(Operation op) {
        operations.add(op);
    }

    public String toString() {
        StringBuilder ret = new StringBuilder();
        if(name != null && !name.isEmpty()) ret.append(name+":\n");
        for(Operation op: operations) {
            ret.append("\t"+op+"\n");
        }
        return ret.toString();
    }

    public List<Byte> toBytes(List<Section> sections) {
        List<Byte> ret = new ArrayList<>();
        int i=offset;
        for(Operation op: operations) {
            ret.addAll(op.toBytes(sections));
            i += op.length();
        }
        return ret;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public byte low() {
        return (byte)(offset << 8 >> 8);
    }

    public byte up() {
        return (byte)(offset >> 8);
    }

    public int getOffset() {
        return offset;
    }

    public int length() {
        int len = 0;
        for(Operation op : operations) {
            int l = op.length();
            if(l == 0){
                System.out.println(op);
            }
            len += op.length();
        }
        return len;
    }

    public String getName() {
        return name;
    }
}