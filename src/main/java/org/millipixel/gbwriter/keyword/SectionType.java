package org.millipixel.gbwriter.keyword;

public abstract class SectionType {
    private final String name;
    private final int start;
    private int offset;

    public SectionType(String name, int start) {
        this.name = name;
        this.start = start;
    }

    public int getAt(int i) {
        return start + i;
    }

    public int getOffset() {
        return offset;
    }

    public String getName() {
        return name;
    }

    public int getStart(){
        return start;
    }
}