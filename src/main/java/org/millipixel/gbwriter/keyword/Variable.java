package org.millipixel.gbwriter.keyword;

public class Variable {
    private final String name;
    private final int address;

    public Variable(String name, int address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public int getAddress() {
        return address;
    }
}
