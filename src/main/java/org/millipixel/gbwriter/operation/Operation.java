package org.millipixel.gbwriter.operation;

import org.millipixel.gbwriter.keyword.Label;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.keyword.Variable;

import java.util.List;

public abstract class Operation {

    public abstract List<Byte> toBytes(List<Section> sections);

    public abstract int length();

    public abstract String toString();

    public Label getLabel(List<Section> sections, String labelName) {
        for(Section section : sections) {
            for(Label label : section.getLabels()) {
                if(label.getName().equals(labelName)) {
                    return label;
                }
            }
        }
        return null;
    }

    public Variable getVariable(List<Section> sections, String variableName) {
        for(Section section : sections) {
            for(Variable variable : section.getVariables()) {
                if(variable.getName().equals(variableName)) {
                    return variable;
                }
            }
        }
        return null;
    }
}