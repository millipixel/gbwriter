package org.millipixel.gbwriter.operation.custom;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.Arrays;
import java.util.List;

public class Dw extends Operation {

    private final String[] str;

    public Dw(String value) {
        str = (value.startsWith("\n") ? value.substring(1) : value).split("\n");
    }

    public Byte[] parse(String str) {
        Byte[] values = new Byte[2];
        char[] chars = str.toCharArray();
        byte b0 = 0;
        byte b1 = 0;
        //01230123 <=> 0123 | 0123 <=> 0011 | 0011 <=> 0101|0101 + 0011|0011 <=> $55,$33`
        //                             0101 | 0101
        for(int j=0; j<8; j++) {
            switch (chars[j]) {
                case '█':
                    b0 |= (byte) (0b1 << (7 - j));
                    b1 |= (byte) (0b1 << (7 - j));
                    break;
                case '▒':
                    b0 |= (byte) (0b0 << (7 - j));
                    b1 |= (byte) (0b1 << (7 - j));
                    break;
                case '░':
                    b0 |= (byte) (0b1 << (7 - j));
                    b1 |= (byte) (0b0 << (7 - j));
                    break;
                case ' ':
                    b0 |= (byte) (0b0 << (7 - j));
                    b1 |= (byte) (0b0 << (7 - j));
                    break;
            }
        }
        values[0] = b0;
        values[1] = b1;
        return values;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        int sx = str[0].length()/8;
        int sy = str.length/8;
        Byte[] values = new Byte[2*8*sx*sy];
        int l = 0;
        for(int i=0; i<sy; i++) {
            for(int j=0; j<sx; j++) {
                for(int k=0; k<8; k++) {
                    Byte[] b = parse(str[k+i*8].substring(j*8, (j+1)*8));
                    values[l++] = b[0];
                    values[l++] = b[1];
                }
            }
        }
        return Arrays.stream(values).toList();
    }

    @Override
    public int length() {
        int sx = str[0].length()/8;
        int sy = str.length/8;
        return 2*8*sx*sy;
    }

    @Override
    public String toString() {
        return "dw \n" + String.join("\n", str);
    }
}
