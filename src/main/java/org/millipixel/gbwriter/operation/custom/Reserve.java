package org.millipixel.gbwriter.operation.custom;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.ArrayList;
import java.util.List;

public class Reserve extends Operation {
    private final byte value;
    private final int length;

    public Reserve(int value, int length) {
        this.length = length;
        this.value = (byte)value;
    }

    public List<Byte> toBytes(List<Section> sections) {
        List<Byte> bytes = new ArrayList<>();
        for(int i=0; i<length; i++) {
            bytes.add(value);
        }
        return bytes;
    }

    @Override
    public int length() {
        return length;
    };

    @Override
    public String toString() {
        return "ds $"+length+" - @, 0";
    };
}
