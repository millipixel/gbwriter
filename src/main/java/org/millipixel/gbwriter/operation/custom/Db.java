package org.millipixel.gbwriter.operation.custom;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.Arrays;
import java.util.List;

public class Db extends Operation {

    private final Byte[] values;

    public Db(int ... values) {
        this.values = new Byte[values.length];
        for(int i=0; i<values.length; i++){
            this.values[i] = (byte) values[i];
        }
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return Arrays.stream(values).toList();
    }

    @Override
    public int length() {
        return values.length;
    }

    @Override
    public String toString() {
        return "db "+ String.join(", ", Arrays.toString(values));
    }
}
