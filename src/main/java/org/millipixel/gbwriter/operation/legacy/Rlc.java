package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Rlc extends Operation {
    private final Id.Identifier r;

    public Rlc(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch (r.name()) {
            case "B" -> List.of((byte) 0xCB, (byte) 0x00);
            case "C" -> List.of((byte) 0xCB, (byte) 0x01);
            case "D" -> List.of((byte) 0xCB, (byte) 0x02);
            case "E" -> List.of((byte) 0xCB, (byte) 0x03);
            case "H" -> List.of((byte) 0xCB, (byte) 0x04);
            case "L" -> List.of((byte) 0xCB, (byte) 0x05);
            case "HLa" -> List.of((byte) 0xCB, (byte) 0x06);
            case "A" -> List.of((byte) 0xCB, (byte) 0x07);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        if(r.name().equals("HLa"))
            return "RLC (HL)";
        else
            return "RLC " + r.name();
    }
}