package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class And extends Operation {

    public final Id.Identifier r;
    public final Byte b;

    public And(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
        this.b = null;
    }

    public And(byte b) {
        this.r = null;
        this.b = b;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        if(r != null && b == null) {
            return switch(r.name()){
                case "A" -> List.of((byte)0xA7);
                case "B" -> List.of((byte)0xA0);
                case "C" -> List.of((byte)0xA1);
                case "D" -> List.of((byte)0xA2);
                case "E" -> List.of((byte)0xA3);
                case "H" -> List.of((byte)0xA4);
                case "L" -> List.of((byte)0xA5);
                case "HLa" -> List.of((byte)0xA6);
                default -> throw new IllegalStateException("Unexpected value: " + r.name());
            };
        } else if(b != null) {
            return List.of((byte)0xE6, b);
        }
        throw new IllegalStateException("Unexpected value: " + r.name() + ", " + b);
    }

    @Override
    public int length() {
        if(r != null && b == null) return 1;
        else return 2;
    }

    @Override
    public String toString() {
        if(r != null && b == null) {
            return switch(r.name()){
                case "A" -> "AND A, A";
                case "B" -> "AND A, B";
                case "C" -> "AND A, C";
                case "D" -> "AND A, D";
                case "E" -> "AND A, E";
                case "H" -> "AND A, H";
                case "L" -> "AND A, L";
                case "HLa" -> "AND A, (HL)";
                default -> "";
            };
        } else if(b != null) {
            return "AND A, $" + Integer.toHexString(b);
        }
        return "";
    }
}
