package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Label;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Jr extends Operation {
    private final Id.Flag flag;
    private final Byte b;

    public Jr(Byte b) {
        this.b = b;
        this.flag = null;
    }

    public Jr(Id.Flag flag, Byte b) {
        this.flag = flag;
        this.b = b;
    }

    public List<Byte> toBytes(List<Section> sections) {
        if(flag == null && b != null) {
            return Arrays.asList((byte)0x18, b);
        } else if(flag != null && b != null) {
            return switch(flag.name()) {
                case "C" -> Arrays.asList((byte)0x38, b);
                case "NC" -> Arrays.asList((byte)0x30, b);
                case "Z" -> Arrays.asList((byte)0x28, b);
                case "NZ" -> Arrays.asList((byte)0x20, b);
                default -> Collections.emptyList();
            };
        }
        return Collections.emptyList();
    }

    @Override
    public int length() {
        return 2;
    };

    @Override
    public String toString() {
        if(flag == null && b != null) {
            return "JR $" + Integer.toHexString(b);
        } else if(flag != null && b != null) {
            return "JR "+flag.name()+", $" + Integer.toHexString(b);
        }
        return "";
    }
}