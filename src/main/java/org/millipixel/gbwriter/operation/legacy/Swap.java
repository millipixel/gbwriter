package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Swap extends Operation {
    private final Id.Identifier r;

    public Swap(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch (r.name()) {
            case "B" -> List.of((byte) 0xCB, (byte) 0x30);
            case "C" -> List.of((byte) 0xCB, (byte) 0x31);
            case "D" -> List.of((byte) 0xCB, (byte) 0x32);
            case "E" -> List.of((byte) 0xCB, (byte) 0x33);
            case "H" -> List.of((byte) 0xCB, (byte) 0x34);
            case "L" -> List.of((byte) 0xCB, (byte) 0x35);
            case "HLa" -> List.of((byte) 0xCB, (byte) 0x36);
            case "A" -> List.of((byte) 0xCB, (byte) 0x37);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        if(r.name().equals("HLa"))
            return "SWAP (HL)";
        else
            return "SWAP " + r.name();
    }
}