package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Rr extends Operation {
    private final Id.Identifier r;

    public Rr(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
    }
    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch (r.name()) {
            case "B" -> List.of((byte) 0xCB, (byte) 0x18);
            case "C" -> List.of((byte) 0xCB, (byte) 0x19);
            case "D" -> List.of((byte) 0xCB, (byte) 0x1A);
            case "E" -> List.of((byte) 0xCB, (byte) 0x1B);
            case "H" -> List.of((byte) 0xCB, (byte) 0x1C);
            case "L" -> List.of((byte) 0xCB, (byte) 0x1D);
            case "HL" -> List.of((byte) 0xCB, (byte) 0x1E);
            case "A" -> List.of((byte) 0xCB, (byte) 0x1F);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        if(r.name().equals("HLa"))
            return "RR (HL)";
        else
            return "RR " + r.name();
    }
}
