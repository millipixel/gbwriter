package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Cp extends Operation {

    public final Id.A_B_C_D_E_H_L_HLa r;
    public final Byte b;

    public Cp(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
        this.b = null;
    }

    public Cp(byte b) {
        this.r = null;
        this.b = b;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        if(r != null) {
            return switch(r.name()){
                case "B" -> List.of((byte)0xB8);
                case "C" -> List.of((byte)0xB9);
                case "D" -> List.of((byte)0xBA);
                case "E" -> List.of((byte)0xBB);
                case "H" -> List.of((byte)0xBC);
                case "L" -> List.of((byte)0xBD);
                case "HLa" -> List.of((byte)0xBE);
                case "A" -> List.of((byte)0xBF);
                default -> List.of();
            };
        } else if(b != null) {
            return List.of((byte)0xFE, b);
        }
        return List.of();
    }

    @Override
    public int length() {
        if(r != null) return 1;
        else return 2;
    }

    @Override
    public String toString() {
        if(r != null) {
            return switch(r.name()){
                case "A" -> "CP A, A";
                case "B" -> "CP A, B";
                case "C" -> "CP A, C";
                case "D" -> "CP A, D";
                case "E" -> "CP A, E";
                case "H" -> "CP A, H";
                case "L" -> "CP A, L";
                case "HLa" -> "CP A, (HL)";
                default -> "";
            };
        } else if(b != null) {
            return "CP A, $" + Integer.toHexString(b);
        }
        return "";
    }
}
