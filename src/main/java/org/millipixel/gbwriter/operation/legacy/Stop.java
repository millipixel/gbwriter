package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Stop extends Operation {
    public List<Byte> toBytes(List<Section> sections) {
        return List.of((byte)0x10, (byte)0x00);
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        return "STOP";
    };

}