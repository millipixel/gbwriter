package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class RST extends Operation {

    private final int i;

    public RST(int i) {
        this.i = i;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch (i) {
            case 0 -> List.of((byte) 0xC7);
            case 1 -> List.of((byte) 0xCF);
            case 2 -> List.of((byte) 0xD7);
            case 3 -> List.of((byte) 0xDF);
            case 4 -> List.of((byte) 0xE7);
            case 5 -> List.of((byte) 0xEF);
            case 6 -> List.of((byte) 0xF7);
            case 7 -> List.of((byte) 0xFF);
            default -> throw new IllegalStateException("Unexpected value: " + i);
        };
    }

    @Override
    public int length() {
        return 1;
    }

    @Override
    public String toString() {
        return "RST " + i;
    }
}
