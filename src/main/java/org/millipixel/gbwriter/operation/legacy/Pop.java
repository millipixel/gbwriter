package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Pop extends Operation {

    public final Id.Identifier r;

    public Pop(Id.BC_DE_HL_AF r) {
        this.r = r;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch(r.name()) {
            case "BC" -> List.of((byte) 0xC1);
            case "DE" -> List.of((byte) 0xD1);
            case "HL" -> List.of((byte) 0xE1);
            case "AF" -> List.of((byte) 0xF1);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 1;
    }

    @Override
    public String toString() {
        return "POP " + r.name();
    }
}
