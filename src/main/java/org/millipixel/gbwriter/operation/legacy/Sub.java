package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Sub extends Operation {

    public final Id.Identifier r;
    public final Byte b;

    public Sub(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
        this.b = null;
    }

    public Sub(byte b) {
        this.r = null;
        this.b = b;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        if(r != null && b == null) {
            return switch(r.name()){
                case "A" -> List.of((byte)0x97);
                case "B" -> List.of((byte)0x90);
                case "C" -> List.of((byte)0x91);
                case "D" -> List.of((byte)0x92);
                case "E" -> List.of((byte)0x93);
                case "H" -> List.of((byte)0x94);
                case "L" -> List.of((byte)0x95);
                case "HLa" -> List.of((byte)0x96);
                default -> throw new IllegalStateException("Unexpected value: " + r.name());
            };
        } else if(b != null) {
            return List.of((byte)0xD6, b);
        }
        throw new IllegalStateException("Unexpected value: " + r.name() + ", " + b);
    }

    @Override
    public int length() {
        if(r != null && b == null) return 1;
        else return 2;
    }

    @Override
    public String toString() {
        if(r != null && b == null) {
            return switch(r.name()){
                case "A" -> "SUB A, A";
                case "B" -> "SUB A, B";
                case "C" -> "SUB A, C";
                case "D" -> "SUB A, D";
                case "E" -> "SUB A, E";
                case "H" -> "SUB A, H";
                case "L" -> "SUB A, L";
                case "HLa" -> "SUB A, (HL)";
                default -> "";
            };
        } else if(b != null) {
            return "SUB A, $" + Integer.toHexString(b);
        }
        return "";
    }
}
