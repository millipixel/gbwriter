package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Ldh extends Operation {

    private final byte b;
    private final boolean left;

    public Ldh(int b, boolean left) {
        this.b = (byte)b;
        this.left = left;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return List.of((byte)(left ? 0xE0 : 0xF0), b);
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        return "LDH" + (left ? Integer.toHexString(b)+", A" : "A, "+Integer.toHexString(b));
    }
}
