package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Bit extends Operation {
    private final Id.Identifier r;
    private final int i;

    public Bit(Id.A_B_C_D_E_H_L_HLa r, int i) {
        this.r = r;
        this.i = i;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch (r.name()) {
            case "B" -> List.of((byte) 0xCB, (byte) (0x40+(i*0x08)));
            case "C" -> List.of((byte) 0xCB, (byte) (0x41+(i*0x08)));
            case "D" -> List.of((byte) 0xCB, (byte) (0x42+(i*0x08)));
            case "E" -> List.of((byte) 0xCB, (byte) (0x43+(i*0x08)));
            case "H" -> List.of((byte) 0xCB, (byte) (0x44+(i*0x08)));
            case "L" -> List.of((byte) 0xCB, (byte) (0x45+(i*0x08)));
            case "HLa" -> List.of((byte) 0xCB, (byte) (0x46+(i*0x08)));
            case "A" -> List.of((byte) 0xCB, (byte) (0x47+(i*0x08)));
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        if(r.name().equals("HLa"))
            return "BIT "+i+", (HL)";
        else
            return "BIT " + i+", "+ r.name();
    }
}