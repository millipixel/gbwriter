package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Inc extends Operation {

    public final Id.Identifier r;

    public Inc(Id.Identifier r) {
        this.r = r;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch(r.name()) {
            case "B" -> List.of((byte)0x04);
            case "D" -> List.of((byte)0x14);
            case "H" -> List.of((byte)0x24);
            case "HLa" -> List.of((byte)0x34);
            case "C" -> List.of((byte)0x0C);
            case "E" -> List.of((byte)0x1C);
            case "L" -> List.of((byte)0x2C);
            case "A" -> List.of((byte)0x3C);
            case "BC" -> List.of((byte)0x03);
            case "DE" -> List.of((byte)0x13);
            case "HL" -> List.of((byte)0x23);
            case "SP" -> List.of((byte)0x33);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 1;
    }

    @Override
    public String toString() {
        return switch(r.name()) {
            case "A" -> "INC A";
            case "B" -> "INC B";
            case "C" -> "INC C";
            case "D" -> "INC D";
            case "E" -> "INC E";
            case "H" -> "INC H";
            case "L" -> "INC L";
            case "BC" -> "INC BC";
            case "DE" -> "INC DE";
            case "HL" -> "INC HL";
            case "HLa" -> "INC (HL)";
            case "SP" -> "INC SP";
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }
}