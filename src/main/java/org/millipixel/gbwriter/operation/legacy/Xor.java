package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Xor extends Operation {

    public final Id.A_B_C_D_E_H_L_HLa r;
    public final Byte b;

    public Xor(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
        this.b = null;
    }

    public Xor(byte b) {
        this.r = null;
        this.b = b;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        if(r != null) {
            return switch(r.name()){
                case "B" -> List.of((byte)0xA8);
                case "C" -> List.of((byte)0xA9);
                case "D" -> List.of((byte)0xAA);
                case "E" -> List.of((byte)0xAB);
                case "H" -> List.of((byte)0xAC);
                case "L" -> List.of((byte)0xAD);
                case "HLa" -> List.of((byte)0xAE);
                case "A" -> List.of((byte)0xAF);
                default -> List.of();
            };
        } else if(b != null) {
            return List.of((byte)0xEE, b);
        }
        return List.of();
    }

    @Override
    public int length() {
        if(r != null) return 1;
        else return 2;
    }

    @Override
    public String toString() {
        if(r != null) {
            return switch(r.name()){
                case "A" -> "XOR A, A";
                case "B" -> "XOR A, B";
                case "C" -> "XOR A, C";
                case "D" -> "XOR A, D";
                case "E" -> "XOR A, E";
                case "H" -> "XOR A, H";
                case "L" -> "XOR A, L";
                case "HLa" -> "XOR A, (HL)";
                default -> "";
            };
        } else if(b != null) {
            return "XOR A, $" + Integer.toHexString(b);
        }
        return "";
    }
}
