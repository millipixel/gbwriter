package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Sla extends Operation {
    private final Id.Identifier r;

    public Sla(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch (r.name()) {
            case "B" -> List.of((byte) 0xCB, (byte) 0x20);
            case "C" -> List.of((byte) 0xCB, (byte) 0x21);
            case "D" -> List.of((byte) 0xCB, (byte) 0x22);
            case "E" -> List.of((byte) 0xCB, (byte) 0x23);
            case "H" -> List.of((byte) 0xCB, (byte) 0x24);
            case "L" -> List.of((byte) 0xCB, (byte) 0x25);
            case "HLa" -> List.of((byte) 0xCB, (byte) 0x26);
            case "A" -> List.of((byte) 0xCB, (byte) 0x27);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        if(r.name().equals("HLa"))
            return "SLA (HL)";
        else
            return "SLA " + r.name();
    }
}