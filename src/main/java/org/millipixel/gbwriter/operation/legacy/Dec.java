package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Dec extends Operation {

    public final Id.Identifier r;

    public Dec(Id.Identifier r) {
        this.r = r;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch(r.name()) {
            case "B" -> List.of((byte)0x05);
            case "D" -> List.of((byte)0x15);
            case "H" -> List.of((byte)0x25);
            case "HLa" -> List.of((byte)0x35);
            case "C" -> List.of((byte)0x0D);
            case "E" -> List.of((byte)0x1D);
            case "L" -> List.of((byte)0x2D);
            case "A" -> List.of((byte)0x3D);
            case "BC" -> List.of((byte)0x0B);
            case "DE" -> List.of((byte)0x1B);
            case "HL" -> List.of((byte)0x2B);
            case "SP" -> List.of((byte)0x3B);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 1;
    }

    @Override
    public String toString() {
        return switch(r.name()) {
            case "A" -> "DEC A";
            case "B" -> "DEC B";
            case "C" -> "DEC C";
            case "D" -> "DEC D";
            case "E" -> "DEC E";
            case "H" -> "DEC H";
            case "L" -> "DEC L";
            case "BC" -> "DEC BC";
            case "DE" -> "DEC DE";
            case "HL" -> "DEC HL";
            case "HLa" -> "DEC (HL)";
            case "SP" -> "DEC SP";
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }
}
