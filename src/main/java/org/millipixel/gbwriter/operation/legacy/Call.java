package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Call extends Operation {

    private final Id.Flag flag;
    private final Integer value;
    private final String label;

    public Call(Id.Flag flag, int value) {
        this.flag = flag;
        this.value = value;
        this.label = null;
    }

    public Call(int value) {
        this.flag = null;
        this.value = value;
        this.label = null;
    }

    public Call(Id.Flag flag, String label) {
        this.flag = flag;
        this.value = null;
        this.label = label;
    }

    public Call(String label) {
        this.flag = null;
        this.value = null;
        this.label = label;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        System.out.println("LABEL " + label);
        byte low = value != null ? (byte)(value << 8 >> 8) : getLabel(sections, label).low();
        byte up = value != null ? (byte)(value >> 8) : getLabel(sections, label).up();
        return switch(flag != null ? flag.name() : null) {
            case "NZ" -> List.of((byte)0xC4, low, up);
            case "NC" -> List.of((byte)0xD4, low, up);
            case "Z" -> List.of((byte)0xCC, low, up);
            case "C" -> List.of((byte)0xDC, low, up);
            case null, default -> List.of((byte)0xCD, low, up);
        };
    }

    @Override
    public int length() {
        return 3;
    }

    @Override
    public String toString() {
        if(value != null) {
            return "CALL " + (flag != null ? flag.name() + ", " : "") + Integer.toHexString(value);
        } else {
            return "CALL " + (flag != null ? flag.name() + ", " : "") + label;
        }
    }
}
