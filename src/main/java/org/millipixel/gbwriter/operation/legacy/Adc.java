package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Adc extends Operation {

    public final Id.A_B_C_D_E_H_L_HLa r;
    public final Byte b;

    public Adc(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
        this.b = null;
    }

    public Adc(byte b) {
        this.r = null;
        this.b = b;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        if(r != null) {
            return switch(r.name()){
                case "B" -> List.of((byte)0x88);
                case "C" -> List.of((byte)0x89);
                case "D" -> List.of((byte)0x8A);
                case "E" -> List.of((byte)0x8B);
                case "H" -> List.of((byte)0x8C);
                case "L" -> List.of((byte)0x8D);
                case "HLa" -> List.of((byte)0x8E);
                case "A" -> List.of((byte)0x8F);
                default -> List.of();
            };
        } else if(b != null) {
            return List.of((byte)0xCE, (byte)b);
        }
        return List.of();
    }

    @Override
    public int length() {
        if(r != null) return 1;
        else return 2;
    }

    @Override
    public String toString() {
        if(r != null) {
            return switch(r.name()){
                case "A" -> "ADC A, A";
                case "B" -> "ADC A, B";
                case "C" -> "ADC A, C";
                case "D" -> "ADC A, D";
                case "E" -> "ADC A, E";
                case "H" -> "ADC A, H";
                case "L" -> "ADC A, L";
                case "HLa" -> "ADC A, (HL)";
                default -> "";
            };
        } else if(b != null) {
            return "ADC A, $" + Integer.toHexString(b);
        }
        return "";
    }
}
