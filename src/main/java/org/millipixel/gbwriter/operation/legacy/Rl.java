package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Rl extends Operation {
    private final Id.Identifier r;

    public Rl(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch (r.name()) {
            case "B" -> List.of((byte) 0xCB, (byte) 0x10);
            case "C" -> List.of((byte) 0xCB, (byte) 0x11);
            case "D" -> List.of((byte) 0xCB, (byte) 0x12);
            case "E" -> List.of((byte) 0xCB, (byte) 0x13);
            case "H" -> List.of((byte) 0xCB, (byte) 0x14);
            case "L" -> List.of((byte) 0xCB, (byte) 0x15);
            case "HLa" -> List.of((byte) 0xCB, (byte) 0x16);
            case "A" -> List.of((byte) 0xCB, (byte) 0x17);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        if(r.name().equals("HLa"))
            return "RL (HL)";
        else
            return "RL " + r.name();
    }
}