package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Label;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

/**
 * Load instruction.
 *
 * @author Sylvain Palominos
 */
public class Ld extends Operation {

    private final Id.Identifier left;
    private final Id.Identifier right;
    private final Integer w;
    private final Byte b;
    private final String label;
    private final String end;

    /**
     * Constructor for instructions
     *  - LD BC,d16
     *  - LD DE,d16
     *  - LD HL,d16
     *  - LD SP,d16
     *
     * @param left Destination register. One of {@link Id.BC}, {@link Id.DE}, {@link Id.HL}, {@link Id.SP}.
     * @param w 16-bit value to load in a register.
     */
    public Ld(Id.BC_DE_HL_SP left, Integer w) {
        this.left = left;
        this.right = null;
        this.w = w;
        this.b = null;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions (with address label)
     *  - LD BC,d16
     *  - LD DE,d16
     *  - LD HL,d16
     *  - LD SP,d16
     *
     * @param left Destination register. One of {@link Id.BC}, {@link Id.DE}, {@link Id.HL}, {@link Id.SP}.
     * @param label Address label.
     */
    public Ld(Id.BC_DE_HL_SP left, String label) {
        this.left = left;
        this.right = null;
        this.w = null;
        this.b = null;
        this.label = label;
        this.end = null;
    }

    /**
     * Constructor for instructions (with start and end address label)
     *  - LD BC,d16
     *  - LD DE,d16
     *  - LD HL,d16
     *  - LD SP,d16
     *
     * @param left Destination register. One of {@link Id.BC}, {@link Id.DE}, {@link Id.HL}, {@link Id.SP}.
     * @param start Start address label.
     * @param end End address label.
     */
    public Ld(Id.BC_DE_HL_SP left, String start, String end) {
        this.left = left;
        this.right = null;
        this.w = null;
        this.b = null;
        this.label = start;
        this.end = end;
    }

    /**
     * Constructor for instructions
     *  - LD (BC),A
     *  - LD (DE),A
     *  - LD (HL+),A
     *  - LD (HL-),A
     *
     * @param left Destination address contained in {@link Id.BC} register.
     * @param right {@link Id.A} register.
     */
    public Ld(Id.BC_DE_HLI_HLD left, Id.A right) {
        this.left = left;
        this.right = right;
        this.w = null;
        this.b = null;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions
     *  - LD A,d8
     *  - LD B,d8
     *  - LD C,d8
     *  - LD D,d8
     *  - LD E,d8
     *  - LD H,d8
     *  - LD L,d8
     *  - LD (HL),d8
     *
     * @param left Destination 8 bit. One of {@link Id.A}, {@link Id.B}, {@link Id.C}, {@link Id.D},
     *            {@link Id.E}, {@link Id.H}, {@link Id.L}, {@link Id.HLa}register.
     * @param b 8-bit value.
     */
    public Ld(Id.A_B_C_D_E_H_L_HLa left, Byte b) {
        this.left = left;
        this.right = null;
        this.w = null;
        this.b = b;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions
     *  - LD (a16),SP
     *
     * @param w Address where the {@link Id.SP} resister is loaded
     * @param right {@link Id.SP} register.
     */
    public Ld(int w, Id.SP right) {
        this.left = null;
        this.right = right;
        this.w = w;
        this.b = null;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions
     *  - LD A,(BC)
     *  - LD A,(DE)
     *  - LD A,(HL+)
     *  - LD A,(HL-)
     *
     * @param left {@link Id.A} register.
     * @param right Destination address contained in {@link Id.BC} register.
     */
    public Ld(Id.A left, Id.BC_DE_HLI_HLD right) {
        this.left = left;
        this.right = right;
        this.w = null;
        this.b = null;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions
     *  - LD A,A    LD B,A    LD C,A    LD D,A    LD E,A    LD H,A    LD L,A
     *  - LD A,B    LD B,B    LD C,B    LD D,B    LD E,B    LD H,B    LD L,B
     *  - LD A,C    LD B,C    LD C,C    LD D,C    LD E,C    LD H,C    LD L,C
     *  - LD A,D    LD B,D    LD C,D    LD D,D    LD E,D    LD H,D    LD L,D
     *  - LD A,E    LD B,E    LD C,E    LD D,E    LD E,E    LD H,E    LD L,E
     *  - LD A,H    LD B,H    LD C,H    LD D,H    LD E,H    LD H,H    LD L,H
     *  - LD A,L    LD B,L    LD C,L    LD D,L    LD E,L    LD H,L    LD L,L
     *  - LD A,(HL) LD B,(HL) LD C,(HL) LD D,(HL) LD E,(HL) LD H,(HL) LD L,(HL)
     *
     * @param left  One of {@link Id.A}, {@link Id.B}, {@link Id.C}, {@link Id.D}, {@link Id.E}, {@link Id.H},
     *              {@link Id.L} register.
     * @param right One of {@link Id.A}, {@link Id.B}, {@link Id.C}, {@link Id.D}, {@link Id.E}, {@link Id.H},
     *              {@link Id.L}, {@link Id.HLa} register.
     */
    public Ld(Id.A_B_C_D_E_H_L left, Id.A_B_C_D_E_H_L_HLa right) {
        this.left = left;
        this.right = right;
        this.w = null;
        this.b = null;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions
     *  - LD (HL),A
     *  - LD (HL),B
     *  - LD (HL),C
     *  - LD (HL),D
     *  - LD (HL),H
     *  - LD (HL),L
     *
     * @param left {@link Id.HLa} register.
     * @param right One of {@link Id.A}, {@link Id.B}, {@link Id.C}, {@link Id.D}, {@link Id.E}, {@link Id.H},
     *              {@link Id.L} register.
     */
    public Ld(Id.HLa left, Id.A_B_C_D_E_H_L right) {
        this.left = left;
        this.right = right;
        this.w = null;
        this.b = null;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions
     *  - LD (C),A
     *
     * @param left {@link Id.C} register.
     * @param right {@link Id.A} register.
     */
    /*public Ld(Id.C left, Id.A right) {
        this.left = left;
        this.right = right;
        this.w = null;
        this.b = null;
        this.label = null;
        this.end = null;
    }*/

    /**
     * Constructor for instructions
     *  - LD A,(C)
     *
     * @param left {@link Id.A} register.
     * @param right {@link Id.C} register.
     */
    /*public Ld(Id.A left, Id.C right) {
        this.left = left;
        this.right = right;
        this.w = null;
        this.b = null;
        this.label = null;
        this.end = null;
    }*/

    /**
     * Constructor for instructions
     *  - LD HL,SP+r8
     *
     * @param left {@link Id.HL} register.
     * @param right {@link Id.SP} register.
     * @param b 8-bit value.
     */
    public Ld(Id.HL left, Id.SP right, byte b) {
        this.left = left;
        this.right = right;
        this.b = b;
        this.w = null;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions
     *  - LD SP,HL
     *
     * @param left {@link Id.SP} register.
     * @param right {@link Id.HL} register.
     */
    public Ld(Id.SP left, Id.HL right) {
        this.left = left;
        this.right = right;
        this.w = null;
        this.b = null;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions
     *  - LD (a16),A
     *
     * @param w 16-bit address.
     * @param right {@link Id.A} register.
     */
    public Ld(int w, Id.A right) {
        this.left = null;
        this.right = right;
        this.w = w;
        this.b = null;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions
     *  - LD A,(a16)
     *
     * @param left {@link Id.A} register.
     * @param w 16-bit address.
     */
    public Ld(Id.A left, Integer w) {
        this.left = left;
        this.right = null;
        this.w = w;
        this.b = null;
        this.label = null;
        this.end = null;
    }

    /**
     * Constructor for instructions (with address label)
     *  - LD A,(a16)
     *
     * @param left {@link Id.A} register.
     * @param label Address label.
     */
    public Ld(Id.A left, String label) {
        this.left = left;
        this.right = null;
        this.w = null;
        this.b = null;
        this.label = label;
        this.end = null;
    }

    /**
     * Constructor for instructions (with start and end address label)
     *  - LD A,(a16)
     *
     * @param left {@link Id.A} register.
     * @param start Start address label.
     * @param end End address label.
     */
    public Ld(Id.A left, String start, String end) {
        this.left = left;
        this.right = null;
        this.w = null;
        this.b = null;
        this.label = start;
        this.end = end;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        // Case of a label/variable or word
        if(w != null || label != null) {
            int value;
            if(w != null) {
                value = w;
            } else if(end == null) {
                value = getLabel(sections, end).getOffset() - getLabel(sections, label).getOffset();
            } else {
                Label l = getLabel(sections, label);
                if(l != null) {
                    value = l.getOffset();
                } else {
                    value = getVariable(sections, label).getAddress();
                }
            }
            if(right instanceof Id.SP) {
                return List.of((byte)0x08, (byte)(value << 8 >> 8), (byte)(value >> 8));
            }
            else if(right instanceof Id.A) {
                return List.of((byte)0xEA, (byte)(value << 8 >> 8), (byte)(value >> 8));
            }
            else if(left != null) {
                return switch(left.name()) {
                    case "A" -> List.of((byte)0xFA, (byte)(value << 8 >> 8), (byte)(value >> 8));
                    case "BC" -> List.of((byte)0x01, (byte)(value << 8 >> 8), (byte)(value >> 8));
                    case "DE" -> List.of((byte)0x11, (byte)(value << 8 >> 8), (byte)(value >> 8));
                    case "HL" -> List.of((byte)0x21, (byte)(value << 8 >> 8), (byte)(value >> 8));
                    case "SP" -> List.of((byte)0x31, (byte)(value << 8 >> 8), (byte)(value >> 8));
                    default -> throw new IllegalStateException("Unexpected value: " + left.name());
                };
            }
        }
        // Case of a byte
        else if(b != null && left != null) {
            return switch(left.name()) {
                case "A" -> List.of((byte)0x3E, b);
                case "B" -> List.of((byte)0x06, b);
                case "C" -> List.of((byte)0x0E, b);
                case "D" -> List.of((byte)0x16, b);
                case "E" -> List.of((byte)0x1E, b);
                case "H" -> List.of((byte)0x26, b);
                case "L" -> List.of((byte)0x2E, b);
                case "HL" -> List.of((byte)0xF8, b);
                case "HLa" -> List.of((byte)0x36, b);
                default -> throw new IllegalStateException("Unexpected value: " + left.name());
            };
        }
        // Case of left and right operand
        else if(left != null && right != null){
            return switch(left.name()) {
                case "A" -> switch (right.name()) {
                    case "A" -> List.of((byte) 0x7F);
                    case "B" -> List.of((byte) 0x78);
                    case "C" -> List.of((byte) 0x79);
                    case "D" -> List.of((byte) 0x7A);
                    case "E" -> List.of((byte) 0x7B);
                    case "H" -> List.of((byte) 0x7C);
                    case "L" -> List.of((byte) 0x7D);
                    case "HLa" -> List.of((byte) 0x7E);
                    case "BC" -> List.of((byte) 0x0A);
                    case "DE" -> List.of((byte) 0x1A);
                    case "HLI" -> List.of((byte) 0x2A);
                    case "HLD" -> List.of((byte) 0x3A);
                    default -> throw new IllegalStateException("Unexpected value: left:" + left + " right:" + right);
                };
                case "B" -> switch (right.name()) {
                    case "A" -> List.of((byte) 0x47);
                    case "B" -> List.of((byte) 0x40);
                    case "C" -> List.of((byte) 0x41);
                    case "D" -> List.of((byte) 0x42);
                    case "E" -> List.of((byte) 0x43);
                    case "H" -> List.of((byte) 0x44);
                    case "L" -> List.of((byte) 0x45);
                    case "HLa" -> List.of((byte) 0x46);
                    default -> throw new IllegalStateException("Unexpected value: left:" + left + " right:" + right);
                };
                case "C" -> switch (right.name()) {
                    case "A" -> List.of((byte) 0x4F);
                    case "B" -> List.of((byte) 0x48);
                    case "C" -> List.of((byte) 0x49);
                    case "D" -> List.of((byte) 0x4A);
                    case "E" -> List.of((byte) 0x4B);
                    case "H" -> List.of((byte) 0x4C);
                    case "L" -> List.of((byte) 0x4D);
                    case "HLa" -> List.of((byte) 0x4E);
                    default -> throw new IllegalStateException("Unexpected value: left:" + left + " right:" + right);
                };
                case "D" -> switch (right.name()) {
                    case "A" -> List.of((byte) 0x57);
                    case "B" -> List.of((byte) 0x50);
                    case "C" -> List.of((byte) 0x51);
                    case "D" -> List.of((byte) 0x52);
                    case "E" -> List.of((byte) 0x53);
                    case "H" -> List.of((byte) 0x54);
                    case "L" -> List.of((byte) 0x55);
                    case "HLa" -> List.of((byte) 0x56);
                    default -> throw new IllegalStateException("Unexpected value: left:" + left + " right:" + right);
                };
                case "E" -> switch (right.name()) {
                    case "A" -> List.of((byte) 0x5F);
                    case "B" -> List.of((byte) 0x58);
                    case "C" -> List.of((byte) 0x59);
                    case "D" -> List.of((byte) 0x5A);
                    case "E" -> List.of((byte) 0x5B);
                    case "H" -> List.of((byte) 0x5C);
                    case "L" -> List.of((byte) 0x5D);
                    case "HLa" -> List.of((byte) 0x5E);
                    default -> throw new IllegalStateException("Unexpected value: left:" + left + " right:" + right);
                };
                case "H" -> switch (right.name()) {
                    case "A" -> List.of((byte) 0x67);
                    case "B" -> List.of((byte) 0x60);
                    case "C" -> List.of((byte) 0x61);
                    case "D" -> List.of((byte) 0x62);
                    case "E" -> List.of((byte) 0x63);
                    case "H" -> List.of((byte) 0x64);
                    case "L" -> List.of((byte) 0x65);
                    case "HLa" -> List.of((byte) 0x66);
                    default -> throw new IllegalStateException("Unexpected value: left:" + left + " right:" + right);
                };
                case "L" -> switch (right.name()) {
                    case "A" -> List.of((byte) 0x6F);
                    case "B" -> List.of((byte) 0x68);
                    case "C" -> List.of((byte) 0x69);
                    case "D" -> List.of((byte) 0x6A);
                    case "E" -> List.of((byte) 0x6B);
                    case "H" -> List.of((byte) 0x6C);
                    case "L" -> List.of((byte) 0x6D);
                    case "HLa" -> List.of((byte) 0x6E);
                    default -> throw new IllegalStateException("Unexpected value: left:" + left + " right:" + right);
                };
                case "HLa" -> switch (right.name()) {
                    case "A" -> List.of((byte) 0x77);
                    case "B" -> List.of((byte) 0x70);
                    case "C" -> List.of((byte) 0x71);
                    case "D" -> List.of((byte) 0x72);
                    case "E" -> List.of((byte) 0x73);
                    case "H" -> List.of((byte) 0x74);
                    case "L" -> List.of((byte) 0x75);
                    default -> throw new IllegalStateException("Unexpected value: left:" + left + " right:" + right);
                };
                case "BC" -> List.of((byte) 0x02);
                case "DE" -> List.of((byte) 0x12);
                case "HLI" -> List.of((byte) 0x22);
                case "HLD" -> List.of((byte) 0x32);
                case "SP" -> List.of((byte) 0xF9);
                default -> throw new IllegalStateException("Unexpected value: " + left.name());
            };
        } else {
            throw new IllegalStateException("Unexpected value: left:" + left + " right:" + right);
        }
        return null;
    }

    @Override
    public int length() {
        if(w != null || label != null) {
            return 3;
        } else if(b != null && left != null) {
            return 2;
        } else if(left != null && right != null){
            return 1;
        } else {
            throw new IllegalStateException("Unexpected value: left:" + left + " right:" + right);
        }
    }

    @Override
    public String toString() {
        // Case of a label/variable or word
        if(w != null) {
            if(left != null) {
                if (left instanceof Id.A)
                    return "LD " + left.name() + ", ($" + Integer.toHexString(w) + ")";
                else
                    return "LD " + left.name() + ", $" + Integer.toHexString(w);
            }
            if(right != null)
                return "LD ($" + Integer.toHexString(w) + "), " + right.name();
        }
        else if(label != null) {
            String value = end != null ? end + " - " + label : label;
            if(left != null)
                return "LD " + left.name()+", "+value;
            if(right != null)
                return "LD " + value + ", " + right.name();
        }
        // Case of a byte
        else if(b != null && left != null && !(right instanceof Id.SP)) {
            if(left instanceof Id.HLa)
                return "LD (HL), $" + Integer.toHexString(b);
            else
                return "LD " + left.name() + ", $" + Integer.toHexString(b);
        }
        // Case of left and right operand
        else if(left != null && right != null){
            if(b != null && left instanceof Id.HL) {
                return "LD HL, SP+$" + Integer.toHexString(b);
            }
            else if(left instanceof Id.BC_DE_HLI_HLD) {
                if(left instanceof Id.HLI)
                    return "LD (HL+), " + right.name();
                else if(left instanceof Id.HLD)
                    return "LD (HL-), " + right.name();
                else
                    return "LD (" + left.name() + "), " + right.name();
            }
            else if(right instanceof Id.BC_DE_HLI_HLD) {
                if(right instanceof Id.HLI)
                    return "LD " + left.name() + ", (HL+)";
                else if(right instanceof Id.HLD)
                    return "LD " + left.name() + ", (HL-)";
                else
                    return "LD " + left.name() + ", (" + right.name() + ")";
            }
            else {
                if(right instanceof Id.HLa)
                    return "LD " + left.name() + ", (HL)";
                else if(left instanceof Id.HLa)
                    return "LD (HL), " + right.name();
                else
                    return "LD " + left.name() + ", " + right.name();
            }
        } else {
            throw new IllegalStateException("Unexpected value: left:" + (left == null ? "null" : left.name()) + " right:" + (right == null ? "null" : right.name()));
        }
        return null;
    }
}
