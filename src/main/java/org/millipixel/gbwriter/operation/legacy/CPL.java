package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class CPL extends Operation {
    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return List.of((byte) 0x2F);
    }

    @Override
    public int length() {
        return 1;
    }

    @Override
    public String toString() {
        return "CPL";
    }
}
