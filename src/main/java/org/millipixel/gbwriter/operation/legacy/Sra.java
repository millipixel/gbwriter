package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Sra extends Operation {
    private final Id.Identifier r;

    public Sra(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
    }
    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch (r.name()) {
            case "B" -> List.of((byte) 0xCB, (byte) 0x28);
            case "C" -> List.of((byte) 0xCB, (byte) 0x29);
            case "D" -> List.of((byte) 0xCB, (byte) 0x2A);
            case "E" -> List.of((byte) 0xCB, (byte) 0x2B);
            case "H" -> List.of((byte) 0xCB, (byte) 0x2C);
            case "L" -> List.of((byte) 0xCB, (byte) 0x2D);
            case "HL" -> List.of((byte) 0xCB, (byte) 0x2E);
            case "A" -> List.of((byte) 0xCB, (byte) 0x2F);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        if(r.name().equals("HLa"))
            return "SRA (HL)";
        else
            return "SRA " + r.name();
    }
}
