package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Or extends Operation {

    public final Id.Identifier r;
    public final Byte b;

    public Or(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
        this.b = null;
    }

    public Or(byte b) {
        this.r = null;
        this.b = b;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        if(r != null && b == null) {
            return switch(r.name()){
                case "A" -> List.of((byte)0xB7);
                case "B" -> List.of((byte)0xB0);
                case "C" -> List.of((byte)0xB1);
                case "D" -> List.of((byte)0xB2);
                case "E" -> List.of((byte)0xB3);
                case "H" -> List.of((byte)0xB4);
                case "L" -> List.of((byte)0xB5);
                case "HLa" -> List.of((byte)0xB6);
                default -> throw new IllegalStateException("Unexpected value: " + r.name());
            };
        } else if(b != null) {
            return List.of((byte)0xF6, b);
        }
        throw new IllegalStateException("Unexpected value: " + r.name() + ", " + b);
    }

    @Override
    public int length() {
        if(r != null && b == null) return 1;
        else return 2;
    }

    @Override
    public String toString() {
        if(r != null && b == null) {
            return switch(r.name()){
                case "A" -> "OR A, A";
                case "B" -> "OR A, B";
                case "C" -> "OR A, C";
                case "D" -> "OR A, D";
                case "E" -> "OR A, E";
                case "H" -> "OR A, H";
                case "L" -> "OR A, L";
                case "HLa" -> "OR A, (HL)";
                default -> "";
            };
        } else if(b != null) {
            return "OR A, $" + Integer.toHexString(b);
        }
        return "";
    }
}
