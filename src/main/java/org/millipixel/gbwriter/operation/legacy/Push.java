package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Push extends Operation {

    public final Id.Identifier r;

    public Push(Id.BC_DE_HL_AF r) {
        this.r = r;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch(r.name()) {
            case "BC" -> List.of((byte) 0xC5);
            case "DE" -> List.of((byte) 0xD5);
            case "HL" -> List.of((byte) 0xE5);
            case "AF" -> List.of((byte) 0xF5);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 1;
    }

    @Override
    public String toString() {
        return "PUSH " + r.name();
    }
}
