package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Sbc extends Operation {

    public final Id.A_B_C_D_E_H_L_HLa r;
    public final Byte b;

    public Sbc(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
        this.b = null;
    }

    public Sbc(byte b) {
        this.r = null;
        this.b = b;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        if(r != null) {
            return switch(r.name()){
                case "B" -> List.of((byte)0x98);
                case "C" -> List.of((byte)0x99);
                case "D" -> List.of((byte)0x9A);
                case "E" -> List.of((byte)0x9B);
                case "H" -> List.of((byte)0x9C);
                case "L" -> List.of((byte)0x9D);
                case "HLa" -> List.of((byte)0x9E);
                case "A" -> List.of((byte)0x9F);
                default -> List.of();
            };
        } else if(b != null) {
            return List.of((byte)0xDE, b);
        }
        return List.of();
    }

    @Override
    public int length() {
        if(r != null) return 1;
        else return 2;
    }

    @Override
    public String toString() {
        if(r != null) {
            return switch(r.name()){
                case "A" -> "SBC A, A";
                case "B" -> "SBC A, B";
                case "C" -> "SBC A, C";
                case "D" -> "SBC A, D";
                case "E" -> "SBC A, E";
                case "H" -> "SBC A, H";
                case "L" -> "SBC A, L";
                case "HLa" -> "SBC A, (HL)";
                default -> "";
            };
        } else if(b != null) {
            return "SBC A, $" + Integer.toHexString(b);
        }
        return "";
    }
}
