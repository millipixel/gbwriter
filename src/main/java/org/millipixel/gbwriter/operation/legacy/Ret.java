package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Ret extends Operation {

    private final Id.Flag flag;

    public Ret(Id.Flag flag) {
        this.flag = flag;
    }

    public Ret() {
        this.flag = null;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch(flag != null ? flag.name() : null) {
            case "NZ" -> List.of((byte)0xC0);
            case "NC" -> List.of((byte)0xD0);
            case "Z" -> List.of((byte)0xC8);
            case "C" -> List.of((byte)0xD8);
            case null, default -> List.of((byte)0xC9);
        };
    }

    @Override
    public int length() {
        return 1;
    }

    @Override
    public String toString() {
        return "RET" + (flag != null ? " " + flag.name() : "");
    }
}
