package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Label;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Jp extends Operation {
    private final Id.Flag flag;
    private final String label;

    public Jp(String label) {
        this.label = label;
        this.flag = null;
    }

    public Jp(Id.Flag flag, String label) {
        this.flag = flag;
        this.label = label;
    }

    public List<Byte> toBytes(List<Section> sections) {
        Label lbl = getLabel(sections, label);
        if(flag == null && label != null) {
            return Arrays.asList((byte)0xC3, lbl.low(), lbl.up());
        } else if(flag != null && label != null) {
            return switch(flag.name()) {
                case "C" -> Arrays.asList((byte)0xDA, lbl.low(), lbl.up());
                case "NC" -> Arrays.asList((byte)0xD2, lbl.low(), lbl.up());
                case "Z" -> Arrays.asList((byte)0xCA, lbl.low(), lbl.up());
                case "NZ" -> Arrays.asList((byte)0xC2, lbl.low(), lbl.up());
                default -> Collections.emptyList();
            };
        }
        return Collections.emptyList();
    }

    @Override
    public int length() {
        return 3;
    };

    @Override
    public String toString() {
        if(flag == null && label != null) {
            return "JP "+label;
        } else if(flag != null && label != null) {
            return "JP "+flag.name()+", "+label;
        }
        return "";
    }
}