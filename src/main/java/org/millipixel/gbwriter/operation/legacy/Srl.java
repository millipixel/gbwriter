package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Srl extends Operation {
    private final Id.Identifier r;

    public Srl(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
    }
    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch (r.name()) {
            case "B" -> List.of((byte) 0xCB, (byte) 0x38);
            case "C" -> List.of((byte) 0xCB, (byte) 0x39);
            case "D" -> List.of((byte) 0xCB, (byte) 0x3A);
            case "E" -> List.of((byte) 0xCB, (byte) 0x3B);
            case "H" -> List.of((byte) 0xCB, (byte) 0x3C);
            case "L" -> List.of((byte) 0xCB, (byte) 0x3D);
            case "HL" -> List.of((byte) 0xCB, (byte) 0x3E);
            case "A" -> List.of((byte) 0xCB, (byte) 0x3F);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        if(r.name().equals("HLa"))
            return "SRL (HL)";
        else
            return "SRL " + r.name();
    }
}
