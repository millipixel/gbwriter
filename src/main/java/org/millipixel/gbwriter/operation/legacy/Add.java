package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Add extends Operation {

    public final Id.Identifier r;
    public final Byte b;

    public Add(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
        this.b = null;
    }

    public Add(byte b) {
        this.r = null;
        this.b = b;
    }

    public Add(Id.SP sp, byte b) {
        this.r = sp;
        this.b = b;
    }

    public Add(Id.HL ignored, Id.BC_DE_HL_SP r) {
        this.r = r;
        this.b = null;
    }

    @Override
    public List<Byte> toBytes(List<Section> sections) {
        if(r != null && b == null) {
            return switch(r.name()){
                case "A" -> List.of((byte)0x87);
                case "B" -> List.of((byte)0x80);
                case "C" -> List.of((byte)0x81);
                case "D" -> List.of((byte)0x82);
                case "E" -> List.of((byte)0x83);
                case "H" -> List.of((byte)0x84);
                case "L" -> List.of((byte)0x85);
                case "HLa" -> List.of((byte)0x86);
                case "BC" -> List.of((byte)0x09);
                case "DE" -> List.of((byte)0x19);
                case "HL" -> List.of((byte)0x29);
                case "SP" -> List.of((byte)0x39);
                default -> throw new IllegalStateException("Unexpected value: " + r.name());
            };
        } else if(b != null) {
            if(r instanceof Id.SP)
                return List.of((byte)0xE8, b);
            else
                return List.of((byte)0xC6, b);
        }
        throw new IllegalStateException("Unexpected value: " + r.name() + ", " + b);
    }

    @Override
    public int length() {
        if(r != null && b == null) return 1;
        else return 2;
    }

    @Override
    public String toString() {
        if(r != null && b == null) {
            return switch(r.name()){
                case "A" -> "ADD A, A";
                case "B" -> "ADD A, B";
                case "C" -> "ADD A, C";
                case "D" -> "ADD A, D";
                case "E" -> "ADD A, E";
                case "H" -> "ADD A, H";
                case "L" -> "ADD A, L";
                case "HLa" -> "ADD A, (HL)";
                case "BC" -> "ADD HL, BC";
                case "DE" -> "ADD HL, DE";
                case "HL" -> "ADD HL, HL";
                case "SP" -> "ADD HL, SP";
                default -> "";
            };
        } else if(b != null) {
            if(r instanceof Id.SP)
                return "ADD SP, $" + Integer.toHexString(b);
            else
                return "ADD A, $" + Integer.toHexString(b);
        }
        return "";
    }
}
