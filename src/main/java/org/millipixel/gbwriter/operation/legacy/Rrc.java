package org.millipixel.gbwriter.operation.legacy;

import org.millipixel.gbwriter.Id;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;

public class Rrc extends Operation {
    private final Id.Identifier r;

    public Rrc(Id.A_B_C_D_E_H_L_HLa r) {
        this.r = r;
    }
    @Override
    public List<Byte> toBytes(List<Section> sections) {
        return switch (r.name()) {
            case "B" -> List.of((byte) 0xCB, (byte) 0x08);
            case "C" -> List.of((byte) 0xCB, (byte) 0x09);
            case "D" -> List.of((byte) 0xCB, (byte) 0x0A);
            case "E" -> List.of((byte) 0xCB, (byte) 0x0B);
            case "H" -> List.of((byte) 0xCB, (byte) 0x0C);
            case "L" -> List.of((byte) 0xCB, (byte) 0x0D);
            case "HL" -> List.of((byte) 0xCB, (byte) 0x0E);
            case "A" -> List.of((byte) 0xCB, (byte) 0x0F);
            default -> throw new IllegalStateException("Unexpected value: " + r.name());
        };
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public String toString() {
        if(r.name().equals("HLa"))
            return "RRC (HL)";
        else
            return "RRC " + r.name();
    }
}
