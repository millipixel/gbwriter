package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Label;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.keyword.Variable;

public interface WithFunctions extends WithLegacyOperations, WithCustomOperations, WithIdentifiers, WithConstants {


    Section getCurrent();

    default void LABEL(String name) {
        Label label = new Label(name);
        getCurrent().addLabel(label);
    }

    default void VAR(String name, int address) {
        Variable variable = new Variable(name, address+getCurrent().getStart()+getCurrent().getSectionType().getStart());
        getCurrent().addVariable(variable);
    }

    default void waitVBlank() {
        calll("WaitVBlank");
    }
    default void declareWaitVBlank() {
        // Do not turn the LCD off outside VBlank
        LABEL("WaitVBlank");
        // Get LCD Y position
        ld(A, LY);
        // Compare with VBlank start Y
        cp(144);
        // If at VBlank, continue
        jp(C, "WaitVBlank");
        ret();
    }

    default void turnLcdOff() {
        ld(A, LCDCF_OFF);
        ld(LCDC, A);
    }

    default void infiniteLoop() {
        LABEL("done");
        jp("done");
    }

    private static int paletteToByte(String palette) {
        int p = 0b0;
        for(int i=0; i<4; i++) {
            switch(palette.charAt(i)) {
                case '█': p |= 0b11 << (3-i)*2;break;
                case '▒': p |= 0b10 << (3-i)*2;break;
                case '░': p |= 0b01 << (3-i)*2;break;
                case ' ': p |= 0b00 << (3-i)*2;break;
            }
        }
        return p;
    }

    default void setBgPalette(String palette) {
        ld(A, paletteToByte(palette));
        ld(BGP, A);
    }

    default void setObj0Palette(String palette) {
        ld(A, paletteToByte(palette));
        ld(OBP0, A);
    }

    default void setObj1Palette(String palette) {
        ld(A, paletteToByte(palette));
        ld(OBP1, A);
    }

    default void setLcd(int value) {
        if(value == 0)
            xor(A);
        else
            ld(A, value);
        ld(LCDC, A);
    }

    default void copyTiles(String from, String to, TileBlock tb) {
        // Copy the tile data
        // Load the start addr, destination addr and the data length in registers
        ld(DE, from);
        ld(HL, tb.getOffset());
        ld(BC, from, to);
        calll("CopyData");
    }

    default void copyTiles(String from, String to, TileBlock tb, int delta) {
        // Copy the tile data
        // Load the start addr, destination addr and the data length in registers
        ld(DE, from);
        ld(HL, tb.getOffset() + delta * 0x10);
        ld(BC, from, to);
        calll("CopyData");
    }

    default void copyTileMap(String from, String to, TileMapBlock tmb) {
        // Copy the tile data
        // Load the start addr, destination addr and the data length in registers
        ld(DE, from);
        ld(HL, tmb.getOffset());
        ld(BC, from, to);
        calll("CopyData");
    }

    default void copyTileMap(String from, String to, TileMapBlock tmb, int delta) {
        // Copy the tile data
        // Load the start addr, destination addr and the data length in registers
        ld(DE, from);
        ld(HL, tmb.getOffset() + delta * 0x10);
        ld(BC, from, to);
        calll("CopyData");
    }

    default void declareCopyData() {

        // Loop on the data to copy it until the end is reached
        LABEL("CopyData");
        ld(A, DE);
        ld(HLI, A);
        inc(DE);
        dec(BC);
        ld(A, B);
        or(C);
        jp(NZ, "CopyData");
        ret();
    }

    default void clearOamRam() {
        ld(A, 0);
        ld(B, 160);
        ld(HL, OAMRAM);
        LABEL("ClearOam");
        ld(HLI, A);
        dec(B);
        jp(NZ, "ClearOam");
    }

    /**
     * Be careful, the (0,0) position is the top-right corner of the screen, without the GB 8px margins.
     * To hide a 2 tile object, call : initObj(-16, -8, 0, 0).
     * @param x
     * @param y
     * @param id
     * @param attr
     */

    default void initObj(int x, int y, int id, int attr) {
        boolean prevXor = false;
        ld(HL, OAMRAM + id*4);
        if (y+8 == 0) {
            xor(A);
            prevXor = true;
        } else {
            ld(A, y+8);
        }
        ld(HLI, A);
        if (x+8 == 0) {
            if(!prevXor) xor(A);
            prevXor = true;
        } else {
            ld(A, x+8);
            prevXor = false;
        }
        ld(HLI, A);
        if (id == 0) {
            if(!prevXor) xor(A);
            prevXor = true;
        } else {
            ld(A, id);
            prevXor = false;
        }
        ld(HLI, A);
        if (attr == 0) {
            if(!prevXor) xor(A);
        } else {
            ld(A, attr);
        }
        ld(HLI, A);
    }
}
