package unnamed

/*import org.millipixel.gbwriter.Program

import static org.millipixel.gbwriter.TileBlock.*
import static org.millipixel.gbwriter.TileMapBlock.*


def pgrm = new Program();
//Header section

pgrm.with {
    SECTION("S_Header", ROM0, 0x100).with {

        nop()
        jp "EntryPoint"

        cartridgeHeader()
        declareWaitVBlank()
        declareCopyData()
    }
    SECTION("S_Main", ROM0) .with {
        LABEL "EntryPoint"
        // Shut down audio circuitry
        //ld A, AUDENA_OFF
        //ld AUDENA, A

        waitVBlank()
        setLcd(LCDCF_OFF)

        LABEL "Tile copy"
        copyTiles("Tiles", "TilesEnd", TILE_BLOCK_3)
        copyTileMap("Tilemap", "TilemapEnd", TILEMAP_BLOCK_1)
        copyTiles("Paddle", "PaddleEnd", TILE_BLOCK_1, 0)
        copyTiles("Ball", "BallEnd", TILE_BLOCK_1, 1)

        LABEL "Object initialization"
        clearOamRam()
        initObj(16, 128, 0, 0)
        initObj(32, 100, 1, 0)

        LABEL "Variables initialization"
        xor A
        ld "wFrameCounter", A

        ld "wCurKeys", A
        ld "wNewKeys", A

        ld A, (byte)-1
        ld "wBallSpeedX", A
        ld A, (byte)-1
        ld "wBallSpeedY", A

        LABEL "Screen settings"
        setLcd(LCDCF_ON | LCDCF_BGON | LCDCF_OBJON)

        setBgPalette "█▒░ "
        setObj0Palette "█▒░ "

        LABEL "Main"
        //Wait next VBlank
        waitVBlank()
        waitVBlank()

        ////////////////
        // Ball moving
        ////////////////
        ld A, "wBallSpeedX"
        ld B, A
        ld A, OAMRAM + 5
        add B
        ld OAMRAM + 5, A

        ld A, "wBallSpeedY"
        ld B, A
        ld A, OAMRAM + 4
        add B
        ld OAMRAM + 4, A

        /////////////////
        // Bouncing
        /////////////////
        LABEL "BounceOnTop"
        // Remember to offset the OAM position!
        // (8, 16) in OAM coordinates is (0, 0) on the screen.
        ld A, OAMRAM + 4
        sub 16 + 1
        ld C, A
        ld A, OAMRAM + 5
        sub 8
        ld B, A
        calll "GetTileByPixel" // Returns tile address in hl
        ld A, HL
        calll "IsWallTile"
        jp NZ, "BounceOnRight"
        ld A, 1
        ld "wBallSpeedY", A

        LABEL "BounceOnRight"
        ld A, OAMRAM + 4
        sub 16
        ld C, A
        ld A, OAMRAM + 5
        sub  8 - 1
        ld B, A
        calll "GetTileByPixel"
        ld A, HL
        calll "IsWallTile"
        jp NZ, "BounceOnLeft"
        ld A, (byte)-1
        ld "wBallSpeedX", A

        LABEL "BounceOnLeft"
        ld A, OAMRAM + 4
        sub 16
        ld C, A
        ld A, OAMRAM + 5
        sub 8 + 1
        ld B, A
        calll "GetTileByPixel"
        ld A, HL
        calll "IsWallTile"
        jp NZ, "BounceOnBottom"
        ld A, 1
        ld "wBallSpeedX", A

        LABEL "BounceOnBottom"
        ld A, OAMRAM + 4
        sub 16 - 1
        ld C, A
        ld A, OAMRAM + 5
        sub 8
        ld B, A
        calll "GetTileByPixel"
        ld A, HL
        calll "IsWallTile"
        jp NZ, "BounceDone"
        ld A, (byte)-1
        ld "wBallSpeedY", A
        LABEL "BounceDone"

        /////////////////////
        // Paddle bouncing
        /////////////////////
        // First, check if the ball is low enough to bounce off the paddle.
        ld A, OAMRAM
        ld B, A
        ld A, OAMRAM + 4
        cp B
        jp NZ, "PaddleBounceDone" // If the ball isn't at the same Y position as the paddle, it can't bounce.
        // Now let's compare the X positions of the objects to see if they're touching.
        ld A, OAMRAM + 5 // Ball's X position.
        ld B, A
        ld A, OAMRAM + 1 // Paddle's X position.
        sub 8
        cp B
        jp NC, "PaddleBounceDone"
        add 8 + 16 // 8 to undo, 16 as the width.
        cp B
        jp C, "PaddleBounceDone"
        ld A, (byte)-1
        ld "wBallSpeedY", A
        LABEL "PaddleBounceDone"

        ///////////////////
        // Move Paddle
        ///////////////////
        calll "UpdateKeys"
        //First, check if the left button is pressed.
        LABEL "CheckLeft"
        ld A, "wCurKeys"
        and PADF_LEFT
        jp Z, "CheckRight"
        //Move the paddle one pixel to the left.
        LABEL "Left"
        ld A, OAMRAM + 1
        dec A
        // If we've already hit the edge of the playfield, don't move.
        cp 15
        jp Z, "Main"
        ld OAMRAM + 1, A
        jp "Main"

        // Then check the right button.
        LABEL "CheckRight"
        ld A, "wCurKeys"
        and PADF_RIGHT
        jp Z, "Main"
        LABEL "Right"
        // Move the paddle one pixel to the right.
        ld A, OAMRAM + 1
        inc A
        // If we've already hit the edge of the playfield, don't move.
        cp 105
        jp Z, "Main"
        ld OAMRAM + 1, A
        jp "Main"

        // Convert a pixel position to a tilemap address
        // hl = $9800 + X + Y * 32
        // @param b: X
        // @param c: Y
        // @return hl: tile address
        LABEL "GetTileByPixel"
        // First, we need to divide by 8 to convert a pixel position to a tile position.
        // After this we want to multiply the Y position by 32.
        // These operations effectively cancel out so we only need to mask the Y value.
        ld A, C
        and 0b11111000
        ld L, A
        ld H, 0
        // Now we have the position * 8 in hl
        add HL, HL // position * 16
        add HL, HL // position * 32
        // Convert the X position to an offset.
        ld A, B
        srl A // a / 2
        srl A // a / 4
        srl A // a / 8
        // Add the two offsets together.
        add L
        ld L, A
        adc H
        sub L
        ld H, A
        // Add the offset to the tilemap's base address, and we are done!
        ld BC, 0x9800
        add HL, BC
        ret()

        // @param a: tile ID
        // @return z: set if a is a wall.
        LABEL "IsWallTile"
        cp 0x00
        ret Z
        cp 0x01
        ret Z
        cp 0x02
        ret Z
        cp 0x03
        ret Z
        cp 0x05
        ret Z
        cp 0x09
        ret()


    }
    SECTION("S_Inputs", ROM0).with {
        LABEL "UpdateKeys"
        // Poll half the controller
        ld A, P1F_GET_BTN
        calll "onenibble"
        ld B, A // B7-4 = 1; B3-0 = unpressed buttons

        // Poll the other half
        ld A, P1F_GET_DPAD
        ld A, P1F_GET_DPAD
        calll "onenibble"
        swap A // A3-0 = unpressed directions; A7-4 = 1
        xor B // A = pressed buttons + directions
        ld B, A // B = pressed buttons + directions

        // And release the controller
        ld A, P1F_GET_NONE
        ldh P1, A

        // Combine with previous wCurKeys to make wNewKeys
        ld A, "wCurKeys"
        xor B // A = keys that changed state
        and B // A = keys that changed to pressed
        ld "wNewKeys", A
        ld A, B
        ld "wCurKeys", A
        ret()

        LABEL "onenibble"
        ldh P1, A // switch the key matrix
        calll "knownret" // burn 10 cycles calling a known ret
        ldh A, P1 // ignore value while waiting for the key matrix to settle
        ldh A, P1
        ldh A, P1 // this read counts
        or 0xF0 // A7-4 = 1; A3-0 = unpressed keys
        LABEL "knownret"
        ret()
    }
    SECTION("S_VarCounter", WRAM0).with {
        VAR "wFrameCounter", 0
    }
    SECTION("S_VarKeys", WRAM0).with {
        VAR "wCurKeys", 1
        VAR "wNewKeys", 2
    }
    SECTION("S_VarBall", WRAM0).with {
        VAR "wBallSpeedX", 3
        VAR "wBallSpeedY", 4
    }
    SECTION("S_ObjTiles", ROM0).with {
        LABEL "Paddle"
        dw """
░██████░
█      █
░██████░
        
        
        
        
        
"""
        LABEL "PaddleEnd"
        LABEL "Ball"
        dw """
   ██   
  █░░█  
  █░░█  
   ██   
        
        
        
        
"""
        LABEL "BallEnd"

    }
    SECTION("S_Tiles", ROM0).with {
        LABEL "Tiles"
        dw """
████████████████████████
████████████████████████
████████████████████████
███▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒███
███▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒███
███▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒███
███▒▒▒░░░░░░░░░░░░▒▒▒███
███▒▒▒░░░░░░░░░░░░▒▒▒███
███▒▒▒░░        ░░▒▒▒███
███▒▒▒░░        ░░▒▒▒███
███▒▒▒░░        ░░▒▒▒███
███▒▒▒░░        ░░▒▒▒███
███▒▒▒░░        ░░▒▒▒███
███▒▒▒░░        ░░▒▒▒███
███▒▒▒░░        ░░▒▒▒███
███▒▒▒░░        ░░▒▒▒███"""
        dw """
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
▒             ▒█
▒ ░░░░░░░░░░░░▒█
▒ ░░░░░░░░░░░░▒█
▒ ░░░░░░░░░░░░▒█
▒ ░░░░░░░░░░░░▒█
▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
████████████████"""
        dw """
████████
████████
████████
████████
████████
████████
████████
████████"""
        dw """
░░  ░░  
░░░░░░░░
░░░░░░░░    
▒░▒░▒░▒░    
▒▒▒▒▒▒▒▒    
▒▒█▒▒▒█▒    
▒█▒█▒█▒█    
████████"""
        LABEL "TilesEnd"

        LABEL "Tilemap"
        db 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x02, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x04, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x04, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x06, 0x07, 0x04, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x05, 0x08, 0x0A, 0x0B, 0x0C, 0x0D, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x05, 0x08, 0x0E, 0x0F, 0x10, 0x11, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x05, 0x08, 0x12, 0x13, 0x14, 0x15, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x05, 0x08, 0x16, 0x17, 0x18, 0x19, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        db 0x03, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x05, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        LABEL "TilemapEnd"
    }
}

println pgrm.toString()

pgrm.writeIn("unnamed.gb")*/