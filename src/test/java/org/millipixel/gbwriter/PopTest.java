package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Pop;

import java.util.List;

public class PopTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Pop(BC), List.of((byte) 0xC1), 1, "POP BC"),
                new Row(new Pop(DE), List.of((byte) 0xD1), 1, "POP DE"),
                new Row(new Pop(HL), List.of((byte) 0xE1), 1, "POP HL"),
                new Row(new Pop(AF), List.of((byte) 0xF1), 1, "POP AF")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
