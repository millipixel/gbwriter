package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Inc;

import java.util.List;

public class IncTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Inc(BC), List.of((byte) 0x03), 1, "INC BC"),
                new Row(new Inc(DE), List.of((byte) 0x13), 1, "INC DE"),
                new Row(new Inc(HL), List.of((byte) 0x23), 1, "INC HL"),
                new Row(new Inc(SP), List.of((byte) 0x33), 1, "INC SP"),

                new Row(new Inc(B), List.of((byte) 0x04), 1, "INC B"),
                new Row(new Inc(D), List.of((byte) 0x14), 1, "INC D"),
                new Row(new Inc(H), List.of((byte) 0x24), 1, "INC H"),
                new Row(new Inc(HLa), List.of((byte) 0x34), 1, "INC (HL)"),

                new Row(new Inc(C), List.of((byte) 0x0C), 1, "INC C"),
                new Row(new Inc(E), List.of((byte) 0x1C), 1, "INC E"),
                new Row(new Inc(L), List.of((byte) 0x2C), 1, "INC L"),
                new Row(new Inc(A), List.of((byte) 0x3C), 1, "INC A")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
