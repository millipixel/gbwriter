package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.*;

import java.util.List;

public class MiscTest extends AbstractTest {

    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Nop(), List.of((byte)0x00), 1, "NOP"),
                new Row(new Stop(), List.of((byte)0x10, (byte)0x00), 2, "STOP"),
                new Row(new DAA(), List.of((byte)0x27), 1, "DAA"),
                new Row(new SCF(), List.of((byte)0x37), 1, "SCF"),
                new Row(new CPL(), List.of((byte)0x2F), 1, "CPL"),
                new Row(new CCF(), List.of((byte)0x3F), 1, "CCF"),
                new Row(new Halt(), List.of((byte)0x76), 1, "HALT"),
                new Row(new Reti(), List.of((byte)0xD9), 1, "RETI"),
                new Row(new EI(), List.of((byte)0xFB), 1, "EI"),
                new Row(new DI(), List.of((byte)0xF3), 1, "DI")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
