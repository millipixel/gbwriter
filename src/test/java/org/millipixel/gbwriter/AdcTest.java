package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Adc;

import java.util.List;

public class AdcTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Adc(B), List.of((byte) 0x88), 1, "ADC A, B"),
                new Row(new Adc(C), List.of((byte) 0x89), 1, "ADC A, C"),
                new Row(new Adc(D), List.of((byte) 0x8A), 1, "ADC A, D"),
                new Row(new Adc(E), List.of((byte) 0x8B), 1, "ADC A, E"),
                new Row(new Adc(H), List.of((byte) 0x8C), 1, "ADC A, H"),
                new Row(new Adc(L), List.of((byte) 0x8D), 1, "ADC A, L"),
                new Row(new Adc(HLa), List.of((byte) 0x8E), 1, "ADC A, (HL)"),
                new Row(new Adc(A), List.of((byte) 0x8F), 1, "ADC A, A"),

                new Row(new Adc((byte)0x12), List.of((byte) 0xCE, (byte) 0x12), 2, "ADC A, $12")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
