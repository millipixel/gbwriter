package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.RLA;
import org.millipixel.gbwriter.operation.legacy.RLCA;
import org.millipixel.gbwriter.operation.legacy.RRA;
import org.millipixel.gbwriter.operation.legacy.RRCA;

import java.util.List;

public class RATest extends AbstractTest{
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new RLCA(), List.of((byte) 0x07), 1, "RLCA"),
                new Row(new RLA(), List.of((byte) 0x17), 1, "RLA"),
                new Row(new RRCA(), List.of((byte) 0x0F), 1, "RRCA"),
                new Row(new RRA(), List.of((byte) 0x1F), 1, "RRA")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
