package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Push;

import java.util.List;

public class PushTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Push(BC), List.of((byte) 0xC5), 1, "PUSH BC"),
                new Row(new Push(DE), List.of((byte) 0xD5), 1, "PUSH DE"),
                new Row(new Push(HL), List.of((byte) 0xE5), 1, "PUSH HL"),
                new Row(new Push(AF), List.of((byte) 0xF5), 1, "PUSH AF")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
