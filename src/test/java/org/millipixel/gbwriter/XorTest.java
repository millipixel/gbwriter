package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Xor;

import java.util.List;

public class XorTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Xor(B), List.of((byte) 0xA8), 1, "XOR A, B"),
                new Row(new Xor(C), List.of((byte) 0xA9), 1, "XOR A, C"),
                new Row(new Xor(D), List.of((byte) 0xAA), 1, "XOR A, D"),
                new Row(new Xor(E), List.of((byte) 0xAB), 1, "XOR A, E"),
                new Row(new Xor(H), List.of((byte) 0xAC), 1, "XOR A, H"),
                new Row(new Xor(L), List.of((byte) 0xAD), 1, "XOR A, L"),
                new Row(new Xor(HLa), List.of((byte) 0xAE), 1, "XOR A, (HL)"),
                new Row(new Xor(A), List.of((byte) 0xAF), 1, "XOR A, A"),

                new Row(new Xor((byte)0x12), List.of((byte) 0xEE, (byte) 0x12), 2, "XOR A, $12")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
