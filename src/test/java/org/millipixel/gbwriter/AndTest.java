package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.And;

import java.util.List;

public class AndTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new And(B), List.of((byte) 0xA0), 1, "AND A, B"),
                new Row(new And(C), List.of((byte) 0xA1), 1, "AND A, C"),
                new Row(new And(D), List.of((byte) 0xA2), 1, "AND A, D"),
                new Row(new And(E), List.of((byte) 0xA3), 1, "AND A, E"),
                new Row(new And(H), List.of((byte) 0xA4), 1, "AND A, H"),
                new Row(new And(L), List.of((byte) 0xA5), 1, "AND A, L"),
                new Row(new And(HLa), List.of((byte) 0xA6), 1, "AND A, (HL)"),
                new Row(new And(A), List.of((byte) 0xA7), 1, "AND A, A"),

                new Row(new And((byte)0x12), List.of((byte) 0xE6, (byte) 0x12), 2, "AND A, $12")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
