package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Label;
import org.millipixel.gbwriter.keyword.Rom0;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Call;
import org.millipixel.gbwriter.operation.legacy.Nop;

import java.util.List;

public class CallTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Call(NZ, "TEST"), List.of((byte) 0xC4, (byte) 0x34, (byte) 0x12), 3, "CALL NZ, TEST"),
                new Row(new Call(NC, "TEST"), List.of((byte) 0xD4, (byte) 0x34, (byte) 0x12), 3, "CALL NC, TEST"),
                new Row(new Call(Z, "TEST"), List.of((byte) 0xCC, (byte) 0x34, (byte) 0x12), 3, "CALL Z, TEST"),
                new Row(new Call(C, "TEST"), List.of((byte) 0xDC, (byte) 0x34, (byte) 0x12), 3, "CALL C, TEST"),
                new Row(new Call("TEST"), List.of((byte) 0xCD, (byte) 0x34, (byte) 0x12), 3, "CALL TEST")
        );
    }

    @Override
    protected List<Section> getSections() {
        Section s = new Section("S", new Rom0());
        for(int i=0; i<0x1234; i++)
            s.addOperation(new Nop());
        s.addLabel(new Label("TEST"));
        return List.of(s);
    }
}
