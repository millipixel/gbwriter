package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Or;

import java.util.List;

public class OrTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Or(B), List.of((byte) 0xB0), 1, "OR A, B"),
                new Row(new Or(C), List.of((byte) 0xB1), 1, "OR A, C"),
                new Row(new Or(D), List.of((byte) 0xB2), 1, "OR A, D"),
                new Row(new Or(E), List.of((byte) 0xB3), 1, "OR A, E"),
                new Row(new Or(H), List.of((byte) 0xB4), 1, "OR A, H"),
                new Row(new Or(L), List.of((byte) 0xB5), 1, "OR A, L"),
                new Row(new Or(HLa), List.of((byte) 0xB6), 1, "OR A, (HL)"),
                new Row(new Or(A), List.of((byte) 0xB7), 1, "OR A, A"),

                new Row(new Or((byte)0x12), List.of((byte) 0xF6, (byte) 0x12), 2, "OR A, $12")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
