package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.RST;

import java.util.List;

public class RstTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new RST(0), List.of((byte) 0xC7), 1, "RST 0"),
                new Row(new RST(1), List.of((byte) 0xCF), 1, "RST 1"),
                new Row(new RST(2), List.of((byte) 0xD7), 1, "RST 2"),
                new Row(new RST(3), List.of((byte) 0xDF), 1, "RST 3"),
                new Row(new RST(4), List.of((byte) 0xE7), 1, "RST 4"),
                new Row(new RST(5), List.of((byte) 0xEF), 1, "RST 5"),
                new Row(new RST(6), List.of((byte) 0xF7), 1, "RST 6"),
                new Row(new RST(7), List.of((byte) 0xFF), 1, "RST 7")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
