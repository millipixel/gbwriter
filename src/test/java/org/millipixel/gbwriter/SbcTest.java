package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Sbc;

import java.util.List;

public class SbcTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Sbc(B), List.of((byte) 0x98), 1, "SBC A, B"),
                new Row(new Sbc(C), List.of((byte) 0x99), 1, "SBC A, C"),
                new Row(new Sbc(D), List.of((byte) 0x9A), 1, "SBC A, D"),
                new Row(new Sbc(E), List.of((byte) 0x9B), 1, "SBC A, E"),
                new Row(new Sbc(H), List.of((byte) 0x9C), 1, "SBC A, H"),
                new Row(new Sbc(L), List.of((byte) 0x9D), 1, "SBC A, L"),
                new Row(new Sbc(HLa), List.of((byte) 0x9E), 1, "SBC A, (HL)"),
                new Row(new Sbc(A), List.of((byte) 0x9F), 1, "SBC A, A"),

                new Row(new Sbc((byte)0x12), List.of((byte) 0xDE, (byte) 0x12), 2, "SBC A, $12")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
