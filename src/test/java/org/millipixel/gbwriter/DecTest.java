package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Dec;

import java.util.List;

public class DecTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Dec(BC), List.of((byte) 0x0B), 1, "DEC BC"),
                new Row(new Dec(DE), List.of((byte) 0x1B), 1, "DEC DE"),
                new Row(new Dec(HL), List.of((byte) 0x2B), 1, "DEC HL"),
                new Row(new Dec(SP), List.of((byte) 0x3B), 1, "DEC SP"),

                new Row(new Dec(B), List.of((byte) 0x05), 1, "DEC B"),
                new Row(new Dec(D), List.of((byte) 0x15), 1, "DEC D"),
                new Row(new Dec(H), List.of((byte) 0x25), 1, "DEC H"),
                new Row(new Dec(HLa), List.of((byte) 0x35), 1, "DEC (HL)"),

                new Row(new Dec(C), List.of((byte) 0x0D), 1, "DEC C"),
                new Row(new Dec(E), List.of((byte) 0x1D), 1, "DEC E"),
                new Row(new Dec(L), List.of((byte) 0x2D), 1, "DEC L"),
                new Row(new Dec(A), List.of((byte) 0x3D), 1, "DEC A")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
