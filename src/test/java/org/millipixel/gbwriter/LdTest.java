package org.millipixel.gbwriter;

import org.junit.jupiter.api.Test;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Ld;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LdTest extends AbstractTest{



    @Override
    protected List<Row> getTestData() {
        return List.of(
            new Row(new Ld(BC, 0x1234), List.of((byte )0x01, (byte) 0x34, (byte) 0x12), 3, "LD BC, $1234"),
            new Row(new Ld(DE, 0x1234), List.of((byte )0x11, (byte) 0x34, (byte) 0x12), 3, "LD DE, $1234"),
            new Row(new Ld(HL, 0x1234), List.of((byte )0x21, (byte) 0x34, (byte) 0x12), 3, "LD HL, $1234"),
            new Row(new Ld(SP, 0x1234), List.of((byte )0x31, (byte) 0x34, (byte) 0x12), 3, "LD SP, $1234"),

            new Row(new Ld(BC, A), List.of((byte) 0x02), 1, "LD (BC), A"),
            new Row(new Ld(DE, A), List.of((byte) 0x012), 1, "LD (DE), A"),
            new Row(new Ld(HLI, A), List.of((byte )0x22), 1, "LD (HL+), A"),
            new Row(new Ld(HLD, A), List.of((byte )0x32), 1, "LD (HL-), A"),

            new Row(new Ld(B, (byte)0x12), List.of((byte) 0x06, (byte) 0x12), 2, "LD B, $12"),
            new Row(new Ld(C, (byte)0x12), List.of((byte) 0x0E, (byte) 0x12), 2, "LD C, $12"),
            new Row(new Ld(D, (byte)0x12), List.of((byte) 0x16, (byte) 0x12), 2, "LD D, $12"),
            new Row(new Ld(E, (byte)0x12), List.of((byte) 0x1E, (byte) 0x12), 2, "LD E, $12"),

            new Row(new Ld(0x1234, SP), List.of((byte) 0x08, (byte) 0x34, (byte) 0x12), 3, "LD ($1234), SP"),

            new Row(new Ld(A, BC), List.of((byte) 0x0A), 1, "LD A, (BC)"),
            new Row(new Ld(A, DE), List.of((byte) 0x1A), 1, "LD A, (DE)"),
            new Row(new Ld(A, HLI), List.of((byte) 0x2A), 1, "LD A, (HL+)"),
            new Row(new Ld(A, HLD), List.of((byte) 0x3A), 1, "LD A, (HL-)"),

            new Row(new Ld(H, (byte)0x12), List.of((byte) 0x26, (byte) 0x12), 2, "LD H, $12"),
            new Row(new Ld(L, (byte)0x12), List.of((byte) 0x2E, (byte) 0x12), 2, "LD L, $12"),
            new Row(new Ld(HLa, (byte)0x12), List.of((byte) 0x36, (byte) 0x12), 2, "LD (HL), $12"),
            new Row(new Ld(A, (byte)0x12), List.of((byte) 0x3E, (byte) 0x12), 2, "LD A, $12"),

            new Row(new Ld(B, B), List.of((byte) 0x40), 1, "LD B, B"),
            new Row(new Ld(B, C), List.of((byte) 0x41), 1, "LD B, C"),
            new Row(new Ld(B, D), List.of((byte) 0x42), 1, "LD B, D"),
            new Row(new Ld(B, E), List.of((byte) 0x43), 1, "LD B, E"),
            new Row(new Ld(B, H), List.of((byte) 0x44), 1, "LD B, H"),
            new Row(new Ld(B, L), List.of((byte) 0x45), 1, "LD B, L"),
            new Row(new Ld(B, HLa), List.of((byte)0x46), 1, "LD B, (HL)"),
            new Row(new Ld(B, A), List.of((byte) 0x47), 1, "LD B, A"),

            new Row(new Ld(C, B), List.of((byte) 0x48), 1, "LD C, B"),
            new Row(new Ld(C, C), List.of((byte) 0x49), 1, "LD C, C"),
            new Row(new Ld(C, D), List.of((byte) 0x4A), 1, "LD C, D"),
            new Row(new Ld(C, E), List.of((byte) 0x4B), 1, "LD C, E"),
            new Row(new Ld(C, H), List.of((byte) 0x4C), 1, "LD C, H"),
            new Row(new Ld(C, L), List.of((byte) 0x4D), 1, "LD C, L"),
            new Row(new Ld(C, HLa), List.of((byte)0x4E), 1, "LD C, (HL)"),
            new Row(new Ld(C, A), List.of((byte) 0x4F), 1, "LD C, A"),

            new Row(new Ld(D, B), List.of((byte) 0x50), 1, "LD D, B"),
            new Row(new Ld(D, C), List.of((byte) 0x51), 1, "LD D, C"),
            new Row(new Ld(D, D), List.of((byte) 0x52), 1, "LD D, D"),
            new Row(new Ld(D, E), List.of((byte) 0x53), 1, "LD D, E"),
            new Row(new Ld(D, H), List.of((byte) 0x54), 1, "LD D, H"),
            new Row(new Ld(D, L), List.of((byte) 0x55), 1, "LD D, L"),
            new Row(new Ld(D, HLa), List.of((byte)0x56), 1, "LD D, (HL)"),
            new Row(new Ld(D, A), List.of((byte) 0x57), 1, "LD D, A"),

            new Row(new Ld(E, B), List.of((byte) 0x58), 1, "LD E, B"),
            new Row(new Ld(E, C), List.of((byte) 0x59), 1, "LD E, C"),
            new Row(new Ld(E, D), List.of((byte) 0x5A), 1, "LD E, D"),
            new Row(new Ld(E, E), List.of((byte) 0x5B), 1, "LD E, E"),
            new Row(new Ld(E, H), List.of((byte) 0x5C), 1, "LD E, H"),
            new Row(new Ld(E, L), List.of((byte) 0x5D), 1, "LD E, L"),
            new Row(new Ld(E, HLa), List.of((byte)0x5E), 1, "LD E, (HL)"),
            new Row(new Ld(E, A), List.of((byte) 0x5F), 1, "LD E, A"),

            new Row(new Ld(H, B), List.of((byte) 0x60), 1, "LD H, B"),
            new Row(new Ld(H, C), List.of((byte) 0x61), 1, "LD H, C"),
            new Row(new Ld(H, D), List.of((byte) 0x62), 1, "LD H, D"),
            new Row(new Ld(H, E), List.of((byte) 0x63), 1, "LD H, E"),
            new Row(new Ld(H, H), List.of((byte) 0x64), 1, "LD H, H"),
            new Row(new Ld(H, L), List.of((byte) 0x65), 1, "LD H, L"),
            new Row(new Ld(H, HLa), List.of((byte)0x66), 1, "LD H, (HL)"),
            new Row(new Ld(H, A), List.of((byte) 0x67), 1, "LD H, A"),

            new Row(new Ld(L, B), List.of((byte) 0x68), 1, "LD L, B"),
            new Row(new Ld(L, C), List.of((byte) 0x69), 1, "LD L, C"),
            new Row(new Ld(L, D), List.of((byte) 0x6A), 1, "LD L, D"),
            new Row(new Ld(L, E), List.of((byte) 0x6B), 1, "LD L, E"),
            new Row(new Ld(L, H), List.of((byte) 0x6C), 1, "LD L, H"),
            new Row(new Ld(L, L), List.of((byte) 0x6D), 1, "LD L, L"),
            new Row(new Ld(L, HLa), List.of((byte)0x6E), 1, "LD L, (HL)"),
            new Row(new Ld(L, A), List.of((byte) 0x6F), 1, "LD L, A"),

            new Row(new Ld(HLa, B), List.of((byte) 0x70), 1, "LD (HL), B"),
            new Row(new Ld(HLa, C), List.of((byte) 0x71), 1, "LD (HL), C"),
            new Row(new Ld(HLa, D), List.of((byte) 0x72), 1, "LD (HL), D"),
            new Row(new Ld(HLa, E), List.of((byte) 0x73), 1, "LD (HL), E"),
            new Row(new Ld(HLa, H), List.of((byte) 0x74), 1, "LD (HL), H"),
            new Row(new Ld(HLa, L), List.of((byte) 0x75), 1, "LD (HL), L"),
            new Row(new Ld(HLa, A), List.of((byte) 0x77), 1, "LD (HL), A"),

            new Row(new Ld(A, B), List.of((byte) 0x78), 1, "LD A, B"),
            new Row(new Ld(A, C), List.of((byte) 0x79), 1, "LD A, C"),
            new Row(new Ld(A, D), List.of((byte) 0x7A), 1, "LD A, D"),
            new Row(new Ld(A, E), List.of((byte) 0x7B), 1, "LD A, E"),
            new Row(new Ld(A, H), List.of((byte) 0x7C), 1, "LD A, H"),
            new Row(new Ld(A, L), List.of((byte) 0x7D), 1, "LD A, L"),
            new Row(new Ld(A, HLa), List.of((byte)0x7E), 1, "LD A, (HL)"),
            new Row(new Ld(A, A), List.of((byte) 0x7F), 1, "LD A, A"),

            new Row(new Ld(HL, SP, (byte)0x12), List.of((byte) 0xF8, (byte) 0x12), 2, "LD HL, SP+$12"),
            new Row(new Ld(SP, HL), List.of((byte) 0xF9), 1, "LD SP, HL"),
            new Row(new Ld(0x1234, A), List.of((byte) 0xEA, (byte) 0x34, (byte) 0x12), 3, "LD ($1234), A"),
            new Row(new Ld(A, 0x1234), List.of((byte) 0xFA, (byte) 0x34, (byte) 0x12), 3, "LD A, ($1234)")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
