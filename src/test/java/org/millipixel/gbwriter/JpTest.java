package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Label;
import org.millipixel.gbwriter.keyword.Rom0;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Jp;
import org.millipixel.gbwriter.operation.legacy.Nop;

import java.util.List;

public class JpTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Jp(NZ, "TEST"), List.of((byte) 0xC2, (byte) 0x34, (byte) 0x12), 3, "JP NZ, TEST"),
                new Row(new Jp(NC, "TEST"), List.of((byte) 0xD2, (byte) 0x34, (byte) 0x12), 3, "JP NC, TEST"),
                new Row(new Jp(Z, "TEST"), List.of((byte) 0xCA, (byte) 0x34, (byte) 0x12), 3, "JP Z, TEST"),
                new Row(new Jp(C, "TEST"), List.of((byte) 0xDA, (byte) 0x34, (byte) 0x12), 3, "JP C, TEST"),
                new Row(new Jp("TEST"), List.of((byte) 0xC3, (byte) 0x34, (byte) 0x12), 3, "JP TEST")
        );
    }

    @Override
    protected List<Section> getSections() {
        Section s = new Section("S", new Rom0());
        for(int i=0; i<0x1234; i++)
            s.addOperation(new Nop());
        s.addLabel(new Label("TEST"));
        return List.of(s);
    }
}
