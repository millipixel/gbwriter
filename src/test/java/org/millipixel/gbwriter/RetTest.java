package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Ret;

import java.util.List;

public class RetTest extends AbstractTest{
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Ret(NZ), List.of((byte) 0xC0), 1, "RET NZ"),
                new Row(new Ret(NC), List.of((byte) 0xD0), 1, "RET NC"),
                new Row(new Ret(Z), List.of((byte) 0xC8), 1, "RET Z"),
                new Row(new Ret(C), List.of((byte) 0xD8), 1, "RET C"),
                new Row(new Ret(), List.of((byte) 0xC9), 1, "RET")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
