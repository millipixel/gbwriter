package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Cp;

import java.util.List;

public class CpTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Cp(B), List.of((byte) 0xB8), 1, "CP A, B"),
                new Row(new Cp(C), List.of((byte) 0xB9), 1, "CP A, C"),
                new Row(new Cp(D), List.of((byte) 0xBA), 1, "CP A, D"),
                new Row(new Cp(E), List.of((byte) 0xBB), 1, "CP A, E"),
                new Row(new Cp(H), List.of((byte) 0xBC), 1, "CP A, H"),
                new Row(new Cp(L), List.of((byte) 0xBD), 1, "CP A, L"),
                new Row(new Cp(HLa), List.of((byte) 0xBE), 1, "CP A, (HL)"),
                new Row(new Cp(A), List.of((byte) 0xBF), 1, "CP A, A"),

                new Row(new Cp((byte)0x12), List.of((byte) 0xFE, (byte) 0x12), 2, "CP A, $12")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
