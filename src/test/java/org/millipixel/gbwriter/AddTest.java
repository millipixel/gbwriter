package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Add;

import java.util.List;

public class AddTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Add(HL, BC), List.of((byte) 0x09), 1, "ADD HL, BC"),
                new Row(new Add(HL, DE), List.of((byte) 0x19), 1, "ADD HL, DE"),
                new Row(new Add(HL, HL), List.of((byte) 0x29), 1, "ADD HL, HL"),
                new Row(new Add(HL, SP), List.of((byte) 0x39), 1, "ADD HL, SP"),

                new Row(new Add(B), List.of((byte) 0x80), 1, "ADD A, B"),
                new Row(new Add(C), List.of((byte) 0x81), 1, "ADD A, C"),
                new Row(new Add(D), List.of((byte) 0x82), 1, "ADD A, D"),
                new Row(new Add(E), List.of((byte) 0x83), 1, "ADD A, E"),
                new Row(new Add(H), List.of((byte) 0x84), 1, "ADD A, H"),
                new Row(new Add(L), List.of((byte) 0x85), 1, "ADD A, L"),
                new Row(new Add(HLa), List.of((byte) 0x86), 1, "ADD A, (HL)"),
                new Row(new Add(A), List.of((byte) 0x87), 1, "ADD A, A"),

                new Row(new Add((byte)0x12), List.of((byte) 0xC6, (byte) 0x12), 2, "ADD A, $12"),
                new Row(new Add(SP, (byte)0x12), List.of((byte) 0xE8, (byte) 0x12), 2, "ADD SP, $12")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
