package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Jr;

import java.util.List;

public class JrTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Jr((byte) 0x12), List.of((byte) 0x18, (byte) 0x12), 2, "JR $12"),
                new Row(new Jr(NZ, (byte) 0x12), List.of((byte) 0x20, (byte) 0x12), 2, "JR NZ, $12"),
                new Row(new Jr(Z, (byte) 0x12), List.of((byte) 0x28, (byte) 0x12), 2, "JR Z, $12"),
                new Row(new Jr(NC, (byte) 0x12), List.of((byte) 0x30, (byte) 0x12), 2, "JR NC, $12"),
                new Row(new Jr(C, (byte) 0x12), List.of((byte) 0x38, (byte) 0x12), 2, "JR C, $12")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
