package org.millipixel.gbwriter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.Operation;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class AbstractTest {
    protected class Row {
        public Operation instr;
        public List<Byte> bytes;
        public int length;
        public String string;
        public Row(final Operation instr, final List<Byte> bytes, final int length, final String string) {
            this.instr = instr;
            this.bytes = bytes;
            this.length = length;
            this.string = string;
        }
    }

    protected Id.BC BC = new Id.BC();
    protected Id.DE DE = new Id.DE();
    protected Id.HL HL = new Id.HL();
    protected Id.HLa HLa = new Id.HLa();
    protected Id.HLI HLI = new Id.HLI();
    protected Id.HLD HLD = new Id.HLD();
    protected Id.SP SP = new Id.SP();
    protected Id.AF AF = new Id.AF();

    protected Id.A A = new Id.A();
    protected Id.B B = new Id.B();
    protected Id.C C = new Id.C();
    protected Id.D D = new Id.D();
    protected Id.E E = new Id.E();
    protected Id.H H = new Id.H();
    protected Id.L L = new Id.L();

    protected Id.NC NC = new Id.NC();
    protected Id.Z Z = new Id.Z();
    protected Id.NZ NZ = new Id.NZ();

    public static void assertByteListEquals(List<Byte> expected, List<Byte> actual) {
        assertEquals(
                expected.stream().map(Integer::toHexString).collect(Collectors.joining(", ")),
                actual.stream().map(Integer::toHexString).collect(Collectors.joining(", ")));
    }

    public static void assertByteListEquals(List<Byte> expected, List<Byte> actual, String message) {
        assertEquals(
                expected.stream().map(LdTest::toHex).collect(Collectors.joining(", ")),
                actual.stream().map(LdTest::toHex).collect(Collectors.joining(", ")), message);
    }

    public static String toHex(int value) {
        String str = Integer.toHexString(value);
        if(str.length()%2 == 1) str = "0"+str;
        return "0x"+str;
    }

    @Test
    public void testInstructions() {
        int off = 0;
        List<Section> sections = getSections();
        for(Section s : sections) {
            off = s.calcOff(off);
        }
        for(Row row : getTestData()) {
            assertEquals(row.string, row.instr.toString(), "Instruction : " + row.instr.toString());
            assertByteListEquals(row.bytes, row.instr.toBytes(sections), "Instruction : " + row.instr.toString());
            assertEquals(row.length, row.instr.length(), "Instruction : " + row.instr.toString());
        }
    }

    protected abstract List<Row> getTestData();
    protected abstract List<Section> getSections();
}
