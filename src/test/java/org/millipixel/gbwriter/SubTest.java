package org.millipixel.gbwriter;

import org.millipixel.gbwriter.keyword.Section;
import org.millipixel.gbwriter.operation.legacy.Sub;

import java.util.List;

public class SubTest extends AbstractTest {
    @Override
    protected List<Row> getTestData() {
        return List.of(
                new Row(new Sub(B), List.of((byte) 0x90), 1, "SUB A, B"),
                new Row(new Sub(C), List.of((byte) 0x91), 1, "SUB A, C"),
                new Row(new Sub(D), List.of((byte) 0x92), 1, "SUB A, D"),
                new Row(new Sub(E), List.of((byte) 0x93), 1, "SUB A, E"),
                new Row(new Sub(H), List.of((byte) 0x94), 1, "SUB A, H"),
                new Row(new Sub(L), List.of((byte) 0x95), 1, "SUB A, L"),
                new Row(new Sub(HLa), List.of((byte) 0x96), 1, "SUB A, (HL)"),
                new Row(new Sub(A), List.of((byte) 0x97), 1, "SUB A, A"),

                new Row(new Sub((byte)0x12), List.of((byte) 0xD6, (byte) 0x12), 2, "SUB A, $12")
        );
    }

    @Override
    protected List<Section> getSections() {
        return List.of();
    }
}
